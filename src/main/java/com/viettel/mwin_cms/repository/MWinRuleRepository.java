package com.viettel.mwin_cms.repository;

import com.viettel.mwin_cms.model.MWinRule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface MWinRuleRepository extends JpaRepository<MWinRule, String> {

    @Query(
            value = "select p from MWinRule p " +
                    "where lower(p.ruleCode) like lower(concat('%', :key, '%')) order by p.ruleCode desc"
    )
    Page<MWinRule> find(Pageable pageable, @Param("key") String key);

}
