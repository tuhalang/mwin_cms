import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';

import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import translationEn from './locales/en/translation.json';
import translationVi from './locales/vi/translation.json';
import productEn from './locales/en/product.json';
import productVi from './locales/vi/product.json';
import sessionEn from './locales/en/session.json';
import sessionVi from './locales/vi/session.json';
import ruleEn from './locales/en/rule.json';
import ruleVi from './locales/vi/rule.json';
import winnerEn from './locales/en/winner.json';
import winnerVi from './locales/vi/winner.json';
import slideshowEn from './locales/en/slideshow.json'
import slideshowVi from './locales/vi/slideshow.json'

const resources = {
    en: {
        translation: translationEn,
        product: productEn,
        session: sessionEn,
        rule: ruleEn,
        winner: winnerEn,
        slideshow: slideshowEn
    },
    vi: {
        translation: translationVi,
        product: productVi,
        session: sessionVi,
        rule: ruleVi,
        winner: winnerVi,
        slideshow: slideshowVi
    }
};

i18n
    // load translation using http -> see /public/locales (i.e. https://github.com/i18next/react-i18next/tree/master/example/react/public/locales)
    // learn more: https://github.com/i18next/i18next-http-backend
    .use(Backend)
    // detect user language
    // learn more: https://github.com/i18next/i18next-browser-languageDetector
    .use(LanguageDetector)
    // pass the i18n instance to react-i18next.
    .use(initReactI18next)
    // init i18next
    // for all options read: https://www.i18next.com/overview/configuration-options
    .init({
        resources,
        fallbackLng: 'en',
        // lng: 'en',
        debug: false,
        ns: ['translation', 'product', 'session', 'winner', 'rule', 'slideshow'],
        defaultNS: 'translation',
        interpolation: {
            escapeValue: false,
        },
        react: {
            useSuspense: false,
        }
    });


export default i18n;
