package com.viettel.mwin_cms.service;

import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.model.MWinTurn;
import org.springframework.data.domain.Pageable;

import java.util.Date;

public interface MWinTurnService {

    void find(ResponseDTO responseDTO, Pageable pageable, String msisdn, Date fromDate, Date endDate) throws Exception;
}
