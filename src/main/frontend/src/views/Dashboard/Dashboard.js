import React from "react";
import ChartistGraph from "react-chartist";
import {makeStyles} from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import {bugs, website, server} from "variables/general.js";

import {
    dailySalesChart,
    emailsSubscriptionChart,
    completedTasksChart
} from "variables/charts.js";

import styles from "assets/jss/cms/views/dashboardStyle.js";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faRegistered,
    faMale,
    faCheckCircle,
    faTimesCircle,
    faDollarSign,
    faUserPlus, faUserTimes
} from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles(styles);

export default function Dashboard() {
    const classes = useStyles();
    return (
        <div>
            <h4>Tổng số sub: 100,000</h4>
            <GridContainer>
                <GridItem xs={12} sm={6} md={3}>
                    <Card>
                        <CardHeader color="success" stats icon>
                            <CardIcon color="success">
                                <FontAwesomeIcon icon={faRegistered}/>
                            </CardIcon>
                            <p className={classes.cardCategory}>Sub đăng kí mới</p>
                            <h3 className={classes.cardTitle}>
                                49
                            </h3>
                        </CardHeader>
                        {/*<CardFooter stats>*/}
                        {/*    <div className={classes.stats}>*/}
                        {/*        /!*<Danger>*!/*/}
                        {/*        /!*    <Warning/>*!/*/}
                        {/*        /!*</Danger>*!/*/}
                        {/*        <a href="#pablo" onClick={e => e.preventDefault()}>*/}
                        {/*            Sub đăng kí mới*/}
                        {/*        </a>*/}
                        {/*    </div>*/}
                        {/*</CardFooter>*/}
                        <CardFooter stats>
                            <div className={classes.stats}>
                                <DateRange/>
                                Last 24 Hours
                            </div>
                        </CardFooter>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                    <Card>
                        <CardHeader color="success" stats icon>
                            <CardIcon color="success">
                                <FontAwesomeIcon icon={faCheckCircle}/>
                            </CardIcon>
                            <p className={classes.cardCategory}>Sub gia hạn thành công</p>
                            <h3 className={classes.cardTitle}>34,245</h3>
                        </CardHeader>
                        <CardFooter stats>
                            <div className={classes.stats}>
                                <DateRange/>
                                Last 24 Hours
                            </div>
                        </CardFooter>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                    <Card>
                        <CardHeader color="danger" stats icon>
                            <CardIcon color="danger">
                                <Icon>info_outline</Icon>
                            </CardIcon>
                            <p className={classes.cardCategory}>Sub hủy</p>
                            <h3 className={classes.cardTitle}>75</h3>
                        </CardHeader>
                        <CardFooter stats>
                            <div className={classes.stats}>
                                <LocalOffer/>
                                Tracked from Github
                            </div>
                        </CardFooter>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                    <Card>
                        <CardHeader color="info" stats icon>
                            <CardIcon color="info">
                                <FontAwesomeIcon icon={faMale}/>
                            </CardIcon>
                            <p className={classes.cardCategory}>Sub chơi lẻ</p>
                            <h3 className={classes.cardTitle}>245</h3>
                        </CardHeader>
                        <CardFooter stats>
                            <div className={classes.stats}>
                                <Update/>
                                Just Updated
                            </div>
                        </CardFooter>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                    <Card>
                        <CardHeader color="success" stats icon>
                            <CardIcon color="success">
                                <FontAwesomeIcon icon={faDollarSign}/>
                            </CardIcon>
                            <p className={classes.cardCategory}>Sub charge cước thành công</p>
                            <h3 className={classes.cardTitle}>245</h3>
                        </CardHeader>
                        <CardFooter stats>
                            <div className={classes.stats}>
                                <Update/>
                                Just Updated
                            </div>
                        </CardFooter>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                    <Card>
                        <CardHeader color="danger" stats icon>
                            <CardIcon color="danger">
                                <FontAwesomeIcon icon={faTimesCircle}/>
                            </CardIcon>
                            <p className={classes.cardCategory}>Sub charge cước không thành công</p>
                            <h3 className={classes.cardTitle}>245</h3>
                        </CardHeader>
                        <CardFooter stats>
                            <div className={classes.stats}>
                                <Update/>
                                Just Updated
                            </div>
                        </CardFooter>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                    <Card>
                        <CardHeader color="info" stats icon>
                            <CardIcon color="info">
                                <FontAwesomeIcon icon={faUserPlus}/>
                            </CardIcon>
                            <p className={classes.cardCategory}>Sub có tham gia chơi</p>
                            <h3 className={classes.cardTitle}>245</h3>
                        </CardHeader>
                        <CardFooter stats>
                            <div className={classes.stats}>
                                <DateRange/>
                                Trong 24 giờ
                            </div>
                        </CardFooter>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                    <Card>
                        <CardHeader color="warning" stats icon>
                            <CardIcon color="warning">
                                <FontAwesomeIcon icon={faUserTimes}/>
                            </CardIcon>
                            <p className={classes.cardCategory}>Sub ko chơi trong ngày</p>
                            <h3 className={classes.cardTitle}>245</h3>
                        </CardHeader>
                        <CardFooter stats>
                            <div className={classes.stats}>
                                <DateRange/>
                                Trong 24 giờ
                            </div>
                        </CardFooter>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
}
