package com.viettel.mwin_cms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MwinCmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MwinCmsApplication.class, args);
    }

}
