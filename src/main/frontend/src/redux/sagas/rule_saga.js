import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import {CREATE_RULE, FETCH_PAGINATE_RULE, UPDATE_RULE} from "../actions/rules/action_types";
import rf from "../../requests/RequestFactory";
import utils from "../../common/utils";
import consts, {BASE_URL} from "../../consts";
import actions from "../actions/rules";
import _ from 'lodash'

function* fetchPaginateRule(action) {
    console.log('=== fetchPaginateRule ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('RuleRequest').fetchPaginateRule(params), action.params);
        console.log(data.data, 'resp');

        if (data.errorCode != 0) {
            utils.showNotification('Rule', data.message, consts.TYPE_ERROR);
            yield put(actions.fetchPaginateRuleFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.fetchPaginateRuleSucceed(data.data));
        console.log(data, 'data =======');
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        console.error(err);
        console.log(err.message, '@: err.message');
        yield put(actions.fetchPaginateRuleFailed(err));
    }
}

function* updateRule(action) {
    console.log('=== updateRule ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('RuleRequest').updateRule(params), action.params);
        console.log(data.data, 'resp');

        if (data.errorCode != 0) {
            utils.showNotification('Rule', data.message, consts.TYPE_ERROR);
            yield put(actions.updateRuleFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.updateRuleSucceed(data.data));
        if (_.isFunction(action.callback)) action.callback()
        console.log(data, 'data =======');
    } catch (err) {
        console.error(err);
        console.log(err.message, '@: err.message');
        yield put(actions.updateRuleFailed(err));
    }
}

function* createRule(action) {
    console.log('=== createRule ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('RuleRequest').createRule(params), action.params);
        console.log(data.data, 'resp');

        if (data.errorCode != 0) {
            utils.showNotification('Rule', data.message, consts.TYPE_ERROR);
            yield put(actions.createRuleFailed(new Error('Error Step 1')));
            return;
        }

        utils.showNotification('Notification', 'Create rule success', 'success')
        if (_.isFunction(action.callback)) action.callback();
        yield put(actions.createRuleSucceed(data.data));
        console.log(data, 'data =======');
    } catch (err) {
        console.error(err);
        console.log(err.message, '@: err.message');
        yield put(actions.createRuleFailed(err));
    }
}

function* watchAllRules() {
    yield takeLatest(FETCH_PAGINATE_RULE, fetchPaginateRule);
    yield takeLatest(UPDATE_RULE, updateRule);
    yield takeLatest(CREATE_RULE, createRule);
}

export default function* rootSaga() {
    yield all([fork(watchAllRules)]);
}
