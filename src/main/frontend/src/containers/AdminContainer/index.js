import {connect} from 'react-redux';
import actions from '../../redux/actions/user'
import actionsHeader from '../../redux/actions/header'
import Admin from "../../layouts/Admin";

const mapStateToProps = (state) => ({
    user: state.user,
    products : state.header.products,
    sessions: state.header.sessions,
});

const mapDispatchToProps = (dispatch) => ({
    logout(params, callback) {
        dispatch(actions.logout(params, callback));
    },
    onSearchProduct(params, callback) {
        dispatch(actionsHeader.searchProduct(params, callback));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
