package com.viettel.mwin_cms.repository;

import com.viettel.mwin_cms.dto.SessionDTO;
import com.viettel.mwin_cms.model.MWinSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface MWinSessionRepository extends JpaRepository<MWinSession, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select new com.viettel.mwin_cms.dto.SessionDTO(s.auctionSessionId, s.startDate, s.endDate, s.status," +
                    " s.sessionNameEn, s.sessionNameLc, s.sessionCode, s.sessionType) " +
                    "from MWinSession s " +
                    "where lower(s.sessionNameEn) like lower(concat('%', :key, '%')) " +
                    " order by s.startDate desc"
    )
    Page<SessionDTO> findByProductNameV2(Pageable pageable, @Param("key") String key);

    @Query(
            value = "select new com.viettel.mwin_cms.dto.SessionDTO(s.auctionSessionId, s.startDate, s.endDate, s.status," +
                    " s.sessionNameEn, s.sessionNameLc, s.sessionCode, s.sessionType) " +
                    "from MWinSession s " +
                    "where s.sessionType = :type and lower(s.sessionNameEn) like lower(concat('%', :key, '%')) " +
                    " order by s.startDate desc"
    )
    Page<SessionDTO> findByProductNameV2AndType(Pageable pageable, @Param("key") String key, @Param("type") String type);


    @Query(
            value = "select new com.viettel.mwin_cms.dto.SessionDTO(s.auctionSessionId, s.startDate, s.endDate, s.status," +
                    " s.sessionNameEn, s.sessionNameLc, s.sessionCode, s.sessionType) " +
                    "from MWinSession s " +
                    "where lower(s.sessionNameEn) like lower(concat('%', :key, '%')) " +
                    "and s.startDate >= :start and s.endDate <= :end " +
                    " order by s.startDate desc"
    )
    Page<SessionDTO> findByRangeDateV2(Pageable pageable, @Param("start") Date start,@Param("key") String key, @Param("end") Date end);

    @Query(
            value = "select new com.viettel.mwin_cms.dto.SessionDTO(s.auctionSessionId, s.startDate, s.endDate, s.status," +
                    " s.sessionNameEn, s.sessionNameLc, s.sessionCode, s.sessionType) " +
                    "from MWinSession s " +
                    "where s.sessionType = :type and lower(s.sessionNameEn) like lower(concat('%', :key, '%')) " +
                    "and s.startDate >= :start and s.endDate <= :end " +
                    " order by s.startDate desc"
    )
    Page<SessionDTO> findByRangeDateV2AndType(Pageable pageable, @Param("start") Date start,@Param("key") String key, @Param("end") Date end, @Param("type") String type);
}
