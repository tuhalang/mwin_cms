package com.viettel.mwin_cms.controller;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.model.MWinSlideshowImage;
import com.viettel.mwin_cms.service.MWinSlideshowImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/secure/slideshows")
public class MWinSlideshowImageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MWinProductController.class);

    @Autowired
    MWinSlideshowImageService mWinSlideshowImageService;

    @PostMapping
    public ResponseEntity create(@RequestBody MWinSlideshowImage mWinSlideshowImage){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            mWinSlideshowImageService.create(responseDTO, mWinSlideshowImage);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody MWinSlideshowImage mWinSlideshowImage){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            mWinSlideshowImageService.update(responseDTO, mWinSlideshowImage);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam Integer status,
                                @RequestParam(required = false) String key){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            mWinSlideshowImageService.find(responseDTO, pageable, status, key);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }

    @DeleteMapping
    public ResponseEntity delete(@RequestParam String id,
                                 @RequestParam Integer type){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            if(type == 1) {
                mWinSlideshowImageService.delete(responseDTO, id);
            }else{
                mWinSlideshowImageService.restore(responseDTO, id);
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }
}
