package com.viettel.mwin_cms.util;

import org.apache.commons.codec.binary.Base64;

import java.io.*;

public class FileUtil {

    static String test = "data:image/png;base64,iVBORw0KGgo";

    public static String getExtFile(String base64code){
        return base64code.split(",")[0].split(";")[0].split("/")[1];
    }

    public static void main(String[] args) {
        System.out.println(getExtFile(test));
    }

    public static String generateFileName(String name, String language, String ext){
        return name + "_" + language + "_" + System.currentTimeMillis() + "." + ext;
    }

    public static void base64ToFile(String base64encode, String path) throws IOException {
        byte[] dataBytes = Base64.decodeBase64(base64encode.getBytes());
        try (OutputStream stream = new FileOutputStream(path)) {
            stream.write(dataBytes);
        }
    }
}

