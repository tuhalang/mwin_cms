import {connect} from 'react-redux';
import actions from '../../redux/actions/history'
import SubCC from "../../views/SubCC";

const mapStateToProps = (state) => ({
    histories: state.history.histories,
    historiesTotal: state.history.historiesTotal
});

const mapDispatchToProps = (dispatch) => ({
    onFetchHistory(params, callback) {
        dispatch(actions.fetchHistory(params, callback));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(SubCC);
