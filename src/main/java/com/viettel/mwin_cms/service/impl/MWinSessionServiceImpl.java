package com.viettel.mwin_cms.service.impl;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.dto.SessionDTO;
import com.viettel.mwin_cms.model.MWinProduct;
import com.viettel.mwin_cms.model.MWinSession;
import com.viettel.mwin_cms.model.MWinSessionProduct;
import com.viettel.mwin_cms.repository.MWinProductRepository;
import com.viettel.mwin_cms.repository.MWinSessionProductRepository;
import com.viettel.mwin_cms.repository.MWinSessionRepository;
import com.viettel.mwin_cms.service.MWinSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MWinSessionServiceImpl implements MWinSessionService {

    @Autowired
    MWinSessionRepository mWinSessionRepository;

    @Autowired
    MWinProductRepository mWinProductRepository;

    @Autowired
    MWinSessionProductRepository mWinSessionProductRepository;

    private boolean isValid(ResponseDTO responseDTO, SessionDTO sessionDTO) {
        return true;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void create(ResponseDTO responseDTO, SessionDTO sessionDTO) throws Exception {
        if (isValid(responseDTO, sessionDTO)) {
            MWinProduct product1 = mWinProductRepository.getOne(sessionDTO.getmWinProductCodes().get(0));
            if (product1 != null) {
                String id = mWinSessionRepository.getID();
                MWinSession mWinSession = new MWinSession();
                mWinSession.setAuctionSessionId(id);
                mWinSession.setStatus(sessionDTO.getStatus());
                mWinSession.setStartDate(sessionDTO.getStartDate());
                mWinSession.setEndDate(sessionDTO.getEndDate());
                mWinSession.setSessionCode(sessionDTO.getSessionCode());
                mWinSession.setSessionType(sessionDTO.getSessionType());
                mWinSession.setSessionNameEn(sessionDTO.getSessionNameEn());
                mWinSession.setSessionNameLc(sessionDTO.getSessionNameLc());

                mWinSessionRepository.save(mWinSession);

                MWinSessionProduct mWinSessionProduct1 = new MWinSessionProduct();
                id = mWinSessionProductRepository.getID();
                mWinSessionProduct1.setId(id);
                mWinSessionProduct1.setProductId(product1.getProductId());
                mWinSessionProduct1.setSessionId(mWinSession.getAuctionSessionId());
                mWinSessionProduct1.setStatus(1);
                mWinSessionProductRepository.save(mWinSessionProduct1);

                if(sessionDTO.getmWinProductCodes().size() > 1){
                    if(!ObjectUtils.isEmpty(sessionDTO.getmWinProductCodes().get(1))) {
                        MWinProduct product2 = mWinProductRepository.getOne(sessionDTO.getmWinProductCodes().get(1));
                        MWinSessionProduct mWinSessionProduct2 = new MWinSessionProduct();
                        id = mWinSessionProductRepository.getID();
                        mWinSessionProduct2.setId(id);
                        mWinSessionProduct2.setProductId(product2.getProductId());
                        mWinSessionProduct2.setSessionId(mWinSession.getAuctionSessionId());
                        mWinSessionProduct2.setStatus(1);
                        mWinSessionProductRepository.save(mWinSessionProduct2);
                    }
                }

                responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
                responseDTO.setMessage(Constant.MSG_SUCCESS);
            }else{
                responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDTO.setMessage(Constant.MSG_NOT_EXISTS);
            }
        }
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void update(ResponseDTO responseDTO, SessionDTO sessionDTO) throws Exception {
        MWinSession oldMWinSession = mWinSessionRepository.findById(sessionDTO.getAuctionSessionId()).orElse(null);
        if (oldMWinSession != null) {
            List<MWinSessionProduct> oldMWinSessionProducts = mWinSessionProductRepository.findAllBySessionId(oldMWinSession.getAuctionSessionId());

            if(oldMWinSessionProducts.size() > 0 && sessionDTO.getmWinProductCodes().size() > 0){
                if(!ObjectUtils.isEmpty(sessionDTO.getmWinProductCodes().get(0))) {
                    MWinProduct product = mWinProductRepository.getOne(sessionDTO.getmWinProductCodes().get(0));
                    if (!oldMWinSessionProducts.get(0).getProductId().equals(product.getProductId())) {
                        oldMWinSessionProducts.get(0).setProductId(product.getProductId());
                    }
                }
            }

            if(oldMWinSessionProducts.size() > 1 && sessionDTO.getmWinProductCodes().size() > 1){
                if(!ObjectUtils.isEmpty(sessionDTO.getmWinProductCodes().get(1))) {
                    MWinProduct product = mWinProductRepository.getOne(sessionDTO.getmWinProductCodes().get(1));
                    if (!oldMWinSessionProducts.get(1).getProductId().equals(product.getProductId())) {
                        oldMWinSessionProducts.get(1).setProductId(product.getProductId());
                    }
                }
            }

            if (!ObjectUtils.isEmpty(sessionDTO.getEndDate())) {
                oldMWinSession.setEndDate(sessionDTO.getEndDate());
            }

            if (!ObjectUtils.isEmpty(sessionDTO.getStartDate())) {
                oldMWinSession.setStartDate(sessionDTO.getStartDate());
            }

            if (!ObjectUtils.isEmpty(sessionDTO.getStatus())
                    && oldMWinSession.getStatus() != sessionDTO.getStatus()) {
                oldMWinSession.setStatus(sessionDTO.getStatus());
            }

            if(!ObjectUtils.isEmpty(sessionDTO.getSessionCode())){
                oldMWinSession.setSessionCode(sessionDTO.getSessionCode());
            }

            if(!ObjectUtils.isEmpty(sessionDTO.getSessionType())){
                oldMWinSession.setSessionType(sessionDTO.getSessionType());
            }

            if(!ObjectUtils.isEmpty(sessionDTO.getSessionNameLc())){
                oldMWinSession.setSessionNameLc(sessionDTO.getSessionNameLc());
            }

            if(!ObjectUtils.isEmpty(sessionDTO.getSessionNameEn())){
                oldMWinSession.setSessionNameEn(sessionDTO.getSessionNameEn());
            }

            mWinSessionRepository.saveAndFlush(oldMWinSession);
            mWinSessionProductRepository.saveAll(oldMWinSessionProducts);
            responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
            responseDTO.setMessage(Constant.MSG_SUCCESS);
        }else{
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_NOT_EXISTS);
        }
    }

    @Override
    public void delete(ResponseDTO responseDTO, String mWinSessionId) throws Exception {
        MWinSession oldMWinSession = mWinSessionRepository.findById(mWinSessionId).orElse(null);
        if(oldMWinSession != null && oldMWinSession.getStatus() == 1){
            oldMWinSession.setStatus(0);
            mWinSessionRepository.saveAndFlush(oldMWinSession);
        }else{
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_NOT_EXISTS);
        }
    }

    @Override
    public void restore(ResponseDTO responseDTO, String mWinSessionId) throws Exception {
        MWinSession oldMWinSession = mWinSessionRepository.findById(mWinSessionId).orElse(null);
        if(oldMWinSession != null && oldMWinSession.getStatus() == 0){
            oldMWinSession.setStatus(1);
            mWinSessionRepository.saveAndFlush(oldMWinSession);
        }else{
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_NOT_EXISTS);
        }
    }

    @Override
    public void findBySessionName(ResponseDTO responseDTO, Pageable pageable, Integer status, String key, String type) throws Exception {
        if(key == null){
            key = "";
        }
        Page<SessionDTO> page = mWinSessionRepository.findByProductNameV2(pageable, key);
        if(!ObjectUtils.isEmpty(type)){
            page = mWinSessionRepository.findByProductNameV2AndType(pageable, key, type);
        }

        List<SessionDTO> mWinSessions = page.getContent().stream().map(s -> {
            s.setmWinProducts(mWinProductRepository.findAllBySessionId(s.getAuctionSessionId()));
            return s;
        }).collect(Collectors.toList());
        responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
        responseDTO.setMessage(Constant.MSG_SUCCESS);
        responseDTO.pushData("items", mWinSessions);
        responseDTO.pushData("totalElements", page.getTotalElements());
        responseDTO.pushData("totalPages", page.getTotalPages());
    }

    @Override
    public void findByRangeDate(ResponseDTO responseDTO, Pageable pageable, Integer status, Date start, Date end, String key, String type) throws Exception {
        Page<SessionDTO> page = mWinSessionRepository.findByRangeDateV2(pageable, start, key, end);
        if(!ObjectUtils.isEmpty(type)){
            page = mWinSessionRepository.findByRangeDateV2AndType(pageable, start, key, end, type);
        }
        List<SessionDTO> mWinSessions = page.getContent().stream().map(s -> {
            s.setmWinProducts(mWinProductRepository.findAllBySessionId(s.getAuctionSessionId()));
            return s;
        }).collect(Collectors.toList());
        responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
        responseDTO.setMessage(Constant.MSG_SUCCESS);
        responseDTO.pushData("items", mWinSessions);
        responseDTO.pushData("totalElements", page.getTotalElements());
        responseDTO.pushData("totalPages", page.getTotalPages());
    }
}
