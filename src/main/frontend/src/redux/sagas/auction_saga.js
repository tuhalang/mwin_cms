import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import utils from "../../common/utils";
import consts, {BASE_URL} from "../../consts";
import actions from "../actions/auction";
import {
    CREATE_SESSION,
    FETCH_PAGINATE_SESSION, FETCH_WINNER_RESULT,
    SEARCH_PAGINATE_PRODUCT,
    UPDATE_SESSION
} from "../actions/auction/action_types";
import _ from 'lodash';

function* searchPaginateProduct(action) {
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('ProductRequest').fetchPaginateProduct(params), action.params);

        if (status === 400) {
            utils.showNotification('Product', data.message, consts.TYPE_ERROR);
            yield put(actions.searchPaginateProductFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.searchPaginateProductSucceed(data.data));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.searchPaginateProductFailed(err));
    }
}

function* fetchPaginateSession(action) {
    console.log('=== fetchPaginateSession ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').fetchPaginateSession(params), action.params);
        console.log('fffffffffffffffffffffff')
        if (data.errorCode != 0) {
            utils.showNotification('Session', data.message, consts.TYPE_ERROR);
            yield put(actions.fetchPaginateSessionFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.fetchPaginateSessionSucceed(data.data));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.fetchPaginateSessionFailed(err));
    }
}

function* updateSession(action) {
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').updateSession(params), action.params);

        if (data.errorCode != 0) {
            utils.showNotification('Session', data.message, consts.TYPE_ERROR);
            yield put(actions.updateSessionFailed(new Error('Error Step 1')));
            return;
        }
        if (_.isFunction(action.callback)) action.callback();
        yield put(actions.updateSessionSucceed(data.data));
    } catch (err) {
        yield put(actions.updateSessionFailed(err));
    }
}

function* createSession(action) {
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').createSession(params), action.params);

        if (data.errorCode != 0) {
            utils.showNotification('Session', data.message, consts.TYPE_ERROR);
            yield put(actions.createSessionFailed(new Error('Error Step 1')));
            return;
        }

        if (_.isFunction(action.callback)) action.callback();
        yield put(actions.createSessionSucceed(data.data));
    } catch (err) {
        yield put(actions.createSessionFailed(err));
    }
}

function* fetchWinnerResult(action) {
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SessionRequest').fetchWinnerResult(params), action.params);

        console.log(data, "data accccc")

        if (data.errorCode != 0) {
            utils.showNotification('Session', data.message, consts.TYPE_ERROR);
            yield put(actions.fetchWinnerResultFailed(new Error('Error Step 1')));
            return;
        }

        if (_.isFunction(action.callback)) action.callback();
        yield put(actions.fetchWinnerResultSucceed(data.data));
    } catch (err) {
        yield put(actions.fetchWinnerResultFailed(err));
    }
}

function* watchAllUsers() {
    yield takeLatest(SEARCH_PAGINATE_PRODUCT, searchPaginateProduct);
    yield takeLatest(FETCH_PAGINATE_SESSION, fetchPaginateSession);
    yield takeLatest(UPDATE_SESSION, updateSession);
    yield takeLatest(CREATE_SESSION, createSession);
    yield takeLatest(FETCH_WINNER_RESULT, fetchWinnerResult);
}

export default function* rootSaga() {
    yield all([fork(watchAllUsers)]);
}
