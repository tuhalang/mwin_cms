package com.viettel.mwin_cms.repository;

import com.viettel.mwin_cms.model.MWinProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MWinProductRepository extends JpaRepository<MWinProduct, String> {

    @Query(
            value = "select p from MWinProduct p " +
                    "where p.status = :status and lower(p.productNameEn) like lower(concat('%', :key, '%')) order by p.productNameEn desc"
    )
    Page<MWinProduct> find(Pageable pageable, @Param("status") Integer status, @Param("key") String key);

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select p from MWinProduct p, MWinSessionProduct sp where p.productId = sp.productId and sp.sessionId = :sessionId order by p.productNameEn"
    )
    List<MWinProduct> findAllBySessionId(@Param("sessionId") String sessionId);

    @Query(
          value = "select p from MWinProduct p where p.productCode = :code order by p.productNameEn"
    )
    MWinProduct findByCode(@Param("code") String code);
}
