import React, {useState, Suspense} from 'react';
import "assets/css/material-dashboard-react.css?v=1.9.0";
import 'antd/dist/antd.css';
import {connect} from 'react-redux';
import {Redirect, Route, Router, Switch, BrowserRouter, HashRouter} from "react-router-dom";
import Admin from "./containers/AdminContainer";
import LoginContainer from "./containers/LoginContainer";
import {createBrowserHistory} from "history";
import Loading from "./components/Loading";

const hist = createBrowserHistory();

function App(props) {
    return (
        <Suspense fallback={<Loading/>}>
            {props.user.isAuthenticated ? <Router history={hist}>
                <Switch>
                    <Route path="/MWIN_CMS/" component={Admin}/>
                    <Redirect from="/" to="/MWIN_CMS/"/>
                </Switch>
            </Router> : <BrowserRouter><LoginContainer/></BrowserRouter>}
        </Suspense>
    );
}

const mapStateToProps = (state) => ({
    user: state.user,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(App);
