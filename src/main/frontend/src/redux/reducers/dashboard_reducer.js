import {
} from '../actions/user/action_types';
import utils from '../../common/utils';

const defaultParams = {
    session: {},
    loginHistories: [],
    userBalances: [],
    isAuthenticated: false,
    userLoginHistory: [],
    userAddressList: [],
    userSystemList: [],
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        default:
            return {
                ...state,
            };
    }
};
