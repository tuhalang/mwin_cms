import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import utils from "../../common/utils";
import consts, {BASE_URL} from "../../consts";
import actions from "../actions/history";
import _ from 'lodash'
import {FETCH_HISTORY} from "../actions/history/action_types";

function* fetchHistory(action) {
    console.log('=== fetchHistory ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('HistoryRequest').fetchHistory(params), action.params);
        console.log(data.data, 'resp');

        if (data.errorCode != 0) {
            utils.showNotification('Rule', data.message, consts.TYPE_ERROR);
            yield put(actions.fetchHistoryFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.fetchHistorySucceed(data.data));
        console.log(data, 'data =======');
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        console.error(err);
        console.log(err.message, '@: err.message');
        yield put(actions.fetchHistoryFailed(err));
    }
}

function* watchAllHistories() {
    yield takeLatest(FETCH_HISTORY, fetchHistory);
}

export default function* rootSaga() {
    yield all([fork(watchAllHistories)]);
}
