package com.viettel.mwin_cms.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "MWIN_AUCTION_SESSION")
public class MWinSession {

    @Id
    @Column(name = "AUCTION_SESSION_ID", length = 200)
    private String auctionSessionId;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "SESSION_NAME_EN")
    private String sessionNameEn;

    @Column(name = "SESSION_NAME_LC")
    private String sessionNameLc;

    @Column(name = "SESSION_CODE")
    private String sessionCode;

    @Column(name = "SESSION_TYPE")
    private String sessionType;

    public String getSessionNameEn() {
        return sessionNameEn;
    }

    public void setSessionNameEn(String sessionNameEn) {
        this.sessionNameEn = sessionNameEn;
    }

    public String getSessionNameLc() {
        return sessionNameLc;
    }

    public void setSessionNameLc(String sessionNameLc) {
        this.sessionNameLc = sessionNameLc;
    }

    public String getSessionCode() {
        return sessionCode;
    }

    public void setSessionCode(String sessionCode) {
        this.sessionCode = sessionCode;
    }

    public String getSessionType() {
        return sessionType;
    }

    public void setSessionType(String sessionType) {
        this.sessionType = sessionType;
    }

    public String getAuctionSessionId() {
        return auctionSessionId;
    }

    public void setAuctionSessionId(String auctionSessionId) {
        this.auctionSessionId = auctionSessionId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
