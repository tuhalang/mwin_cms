package com.viettel.mwin_cms.service;

import com.viettel.mwin_cms.dto.AccountDTO;
import com.viettel.mwin_cms.dto.ResponseDTO;

public interface UserService {

    void signIn(ResponseDTO responseDTO, AccountDTO accountDTO) throws Exception;
}
