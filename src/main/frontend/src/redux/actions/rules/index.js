import {
    CREATE_RULE,
    CREATE_RULE_FAILED,
    CREATE_RULE_SUCCEED,
    FETCH_PAGINATE_RULE,
    FETCH_PAGINATE_RULE_FAILED,
    FETCH_PAGINATE_RULE_SUCCEED,
    UPDATE_RULE,
    UPDATE_RULE_RULE_FAILED,
    UPDATE_RULE_RULE_SUCCEED
} from './action_types';

export default {

    fetchPaginateRule: (params, callback) => ({
        type: FETCH_PAGINATE_RULE,
        params,
        callback,
    }),
    fetchPaginateRuleSucceed: (data) => ({
        type: FETCH_PAGINATE_RULE_SUCCEED,
        data,
    }),
    fetchPaginateRuleFailed: (err) => ({
        type: FETCH_PAGINATE_RULE_FAILED,
        err,
    }),

    createRule: (params, callback) => ({
        type: CREATE_RULE,
        params,
        callback,
    }),
    createRuleSucceed: (data) => ({
        type: CREATE_RULE_SUCCEED,
        data,
    }),
    createRuleFailed: (err) => ({
        type: CREATE_RULE_FAILED,
        err,
    }),

    updateRule: (params, callback) => ({
        type: UPDATE_RULE,
        params,
        callback,
    }),
    updateRuleSucceed: (data) => ({
        type: UPDATE_RULE_RULE_SUCCEED,
        data,
    }),
    updateRuleFailed: (err) => ({
        type: UPDATE_RULE_RULE_FAILED,
        err,
    }),

};
