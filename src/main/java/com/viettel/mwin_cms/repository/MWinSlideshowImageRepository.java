package com.viettel.mwin_cms.repository;

import com.viettel.mwin_cms.model.MWinSlideshowImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MWinSlideshowImageRepository extends JpaRepository<MWinSlideshowImage, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select p from MWinSlideshowImage p " +
                    "where lower(p.imageTitleEn) like lower(concat('%', :key, '%')) order by p.imageOrder"
    )
    Page<MWinSlideshowImage> find(Pageable pageable, @Param("key") String key);
}
