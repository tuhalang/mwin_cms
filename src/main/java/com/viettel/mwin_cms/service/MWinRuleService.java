package com.viettel.mwin_cms.service;

import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.model.MWinRule;
import org.springframework.data.domain.Pageable;

public interface MWinRuleService {

    void create(ResponseDTO responseDTO, MWinRule mWinRule) throws Exception;
    void update(ResponseDTO responseDTO, MWinRule mWinRule) throws Exception;
    void delete(ResponseDTO responseDTO, String mWinRuleId) throws Exception;
    void restore(ResponseDTO responseDTO, String mWinRuleId) throws Exception;
    void find(ResponseDTO responseDTO, Pageable pageable, String key) throws Exception;

}
