import {connect} from 'react-redux';
import actions from '../../redux/actions/auction'
import Winner from "../../views/Winners";

const mapStateToProps = (state) => ({
    sessions: state.auction.sessions,
    winnerResult: state.auction.winnerResult,
});

const mapDispatchToProps = (dispatch) => ({
    onFetchPaginateSession: (params, callback) => {
        dispatch(actions.fetchPaginateSession(params, callback))
    },
    onFetchWinnerResult: (params, callback) => {
        dispatch(actions.fetchWinnerResult(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Winner);
