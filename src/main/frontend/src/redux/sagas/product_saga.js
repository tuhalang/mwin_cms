import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import {CREATE_PRODUCT, FETCH_PAGINATE_PRODUCT, UPDATE_PRODUCT} from "../actions/product/action_types";
import rf from "../../requests/RequestFactory";
import utils from "../../common/utils";
import consts, {BASE_URL} from "../../consts";
import actions from "../actions/product";
import _ from 'lodash'

function* fetchPaginateProduct(action) {
    console.log('=== fetchPaginateProduct ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('ProductRequest').fetchPaginateProduct(params), action.params);
        console.log(data.data, 'resp');

        if (data.errorCode != 0) {
            utils.showNotification('Product', data.message, consts.TYPE_ERROR);
            yield put(actions.fetchPaginateProductFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.fetchPaginateProductSucceed(data.data));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.fetchPaginateProductFailed(err));
    }
}

function* updateProduct(action) {
    try {
        const {
            data,
            status
        } = yield call((params, query) => rf.getRequest('ProductRequest').updateProduct(params, query), action.params.mWinProduct, action.params.params);

        if (data.errorCode != 0) {
            utils.showNotification('Product', data.message, consts.TYPE_ERROR);
            yield put(actions.updateProductFailed(new Error('Error Step 1')));
            return;
        }
        if (_.isFunction(action.callback)) action.callback();
        yield put(actions.updateProductSucceed(data.data));
    } catch (err) {
        yield put(actions.updateProductFailed(err));
    }
}

function* createProduct(action) {
    try {
        const {
            data,
            status
        } = yield call((params, query) => rf.getRequest('ProductRequest').createProduct(params, query), action.params.mWinProduct, action.params.params);

        if (data.errorCode != 0) {
            utils.showNotification('Product', data.message, consts.TYPE_ERROR);
            yield put(actions.createProductFailed(new Error('Error Step 1')));
            return;
        }

        if (_.isFunction(action.callback)) action.callback();
        yield put(actions.createProductSucceed(data.data));
    } catch (err) {
        yield put(actions.createProductFailed(err));
    }
}

function* watchAllProducts() {
    yield takeLatest(FETCH_PAGINATE_PRODUCT, fetchPaginateProduct);
    yield takeLatest(UPDATE_PRODUCT, updateProduct);
    yield takeLatest(CREATE_PRODUCT, createProduct);
}

export default function* rootSaga() {
    yield all([fork(watchAllProducts)]);
}
