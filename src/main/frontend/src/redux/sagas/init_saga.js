import {
  all, fork,
} from 'redux-saga/effects';

function* watchInit() {
}

export default function* rootSaga() {
  yield all([fork(watchInit)]);
}
