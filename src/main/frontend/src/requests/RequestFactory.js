import LoginRequest from './LoginRequest';
import DashBoardRequest from "./DashBoardRequest";
import WinnersRequest from "./WinnersRequest";
import ProductRequest from './ProductRequest'
import RuleRequest from './RuleRequest'
import SessionRequest from './SessionRequest'
import HistoryRequest from "./HistoryRequest";
import SlideshowRequest from './SlideshowRequest';

const requestMap = {
    LoginRequest,
    DashBoardRequest,
    WinnersRequest,
    ProductRequest,
    RuleRequest,
    SessionRequest,
    HistoryRequest,
    SlideshowRequest
};

const instances = {};

export default class RequestFactory {
    static getRequest(classname) {
        const RequestClass = requestMap[classname];
        if (!RequestClass) {
            throw new Error(`Invalid request class name: ${classname}`);
        }

        let requestInstance = instances[classname];
        if (!requestInstance) {
            requestInstance = new RequestClass();
            instances[classname] = requestInstance;
        }
        return requestInstance;
    }
}
