import {
    put, takeLatest, call, all, fork,
} from 'redux-saga/effects';

import actions from '../actions/user';
import rf from '../../requests/RequestFactory';
import _ from 'lodash';
import consts, {BASE_URL} from '../../consts';
import utils from '../../common/utils';
import {LOGIN_HISTORY, LOGIN_NORMAL, LOGOUT} from "../actions/user/action_types";
import axios from "axios";


function* checkAuthSession(action) {
    console.log('=== checkAuthSession ===');
    const strSession = window.localStorage.getItem('session');
    // check token expired
    //if (!!session && moment(session.time_expired / 1000).isAfter()) {
    if (strSession != null && strSession != '') {
        const session = JSON.parse(strSession || '{}');
        console.log(session, "session true")
        // add token for axios
        window.axios = axios.create({
            baseURL: BASE_URL,
            headers: {
                Authorization: 'Bearer  ' + session.token,
            },
        });
        if (_.isFunction(action.callback)) action.callback()
        yield put(actions.loginNormalActionSucceed(session));
    }
    yield put(actions.loginNormalActionFailed());
}

function* loginNormal(action) {
    console.log('=== loginNormal ===', action);
    try {
        const {data, status} = yield call((params) => rf.getRequest('LoginRequest').loginNormal(params), action.params);
        console.log(data.data, 'resp');

        if (status === 400) {
            utils.showNotification('Login', data.message, consts.TYPE_ERROR);
            yield put(actions.loginHistoryActionFailed(new Error('Error Step 1')));
            return;
        }
        window.axios = axios.create({
            baseURL: BASE_URL,
            headers: {
                Authorization: 'Bearer  ' + data.data.token,
            },
        });
        yield window.localStorage.setItem('session', JSON.stringify(data.data));

        yield put(actions.loginNormalActionSucceed(data));
        console.log(data, 'data =======');
    } catch (err) {
        console.error(err);
        console.log(err.message, '@: err.message');
        yield put(actions.loginNormalActionFailed(err));
    }
}

function* logout(action) {
    console.log("logout saga")
    try {
        // const {data} = yield call((params) => rf.getRequest('LoginRequest').logout(params), action.params.data);
        window.localStorage.setItem('session', '');
        if (_.isFunction(action.callback)) action.callback()
        yield put(actions.logoutSucceed());
    } catch (err) {
        console.error(err);
        console.log(err.message, '@: err.message');
        yield put(actions.logoutFailed(err));
    }
}

function* watchAllUsers() {
    yield takeLatest(LOGIN_HISTORY, checkAuthSession);
    yield takeLatest(LOGIN_NORMAL, loginNormal);
    yield takeLatest(LOGOUT, logout);
}

export default function* rootSaga() {
    yield all([fork(watchAllUsers)]);
}
