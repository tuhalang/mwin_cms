package com.viettel.mwin_cms.dto;

import com.viettel.mwin_cms.common.Constant;

import java.util.LinkedHashMap;

public class ResponseDTO {

    private String errorCode;
    private String message;
    private LinkedHashMap<String, Object> data;

    public ResponseDTO() {
        this.errorCode = Constant.ERROR_CODE_NOK;
    }

    public void pushData(String key, Object value){
        if(this.data == null){
            this.data = new LinkedHashMap<>();
        }
        this.data.put(key, value);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LinkedHashMap<String, Object> getData() {
        return data;
    }

    public void setData(LinkedHashMap<String, Object> data) {
        this.data = data;
    }
}
