import {all} from 'redux-saga/effects';
import watchInit from './init_saga';
import watchDashBoard from './dashboard_saga'
import watchAllUsers from './user_saga'
import watchProduct from './product_saga'
import watchAuction from './auction_saga'
import watchRule from './rule_saga'
import watchHeader from './header_saga'
import watchAllHistories from './history_saga';
import watchAllSlideshow from './slideshow_saga';

function* rootSaga() {
    yield all([
        // watchInit(),
        // watchDashBoard(),
        watchAllUsers(),
        watchProduct(),
        watchAuction(),
        watchRule(),
        watchHeader(),
        watchAllHistories(),
        watchAllSlideshow(),
    ]);
}

export default rootSaga;
