import {
    FETCH_PAGINATE_SLIDESHOW,
    FETCH_PAGINATE_SLIDESHOW_SUCCEED,
    FETCH_PAGINATE_SLIDESHOW_FAILED,
    CREATE_SLIDESHOW,
    UPDATE_SLIDESHOW,
    UPDATE_SLIDESHOW_SLIDESHOW_SUCCEED,
    UPDATE_SLIDESHOW_SLIDESHOW_FAILED,
    DELETE_SLIDESHOW,
    DELETE_SLIDESHOW_SLIDESHOW_SUCCEED,
    DELETE_SLIDESHOW_SLIDESHOW_FAILED,
    CREATE_SLIDESHOW_SUCCEED,
    CREATE_SLIDESHOW_FAILED
} from './action_types';

export default {

    fetchPaginateSlideshow: (params, callback) => ({
        type: FETCH_PAGINATE_SLIDESHOW,
        params,
        callback,
    }),
    fetchPaginateSlideshowSucceed: (data) => ({
        type: FETCH_PAGINATE_SLIDESHOW_SUCCEED,
        data,
    }),
    fetchPaginateSlideshowFailed: (err) => ({
        type: FETCH_PAGINATE_SLIDESHOW_FAILED,
        err,
    }),

    createSlideshow: (params, callback) => ({
        type: CREATE_SLIDESHOW,
        params,
        callback,
    }),
    createSlideshowSucceed: (data) => ({
        type: CREATE_SLIDESHOW_SUCCEED,
        data,
    }),
    createSlideshowFailed: (err) => ({
        type: CREATE_SLIDESHOW_FAILED,
        err,
    }),

    updateSlideshow: (params, callback) => ({
        type: UPDATE_SLIDESHOW,
        params,
        callback,
    }),
    updateSlideshowSucceed: (data) => ({
        type: UPDATE_SLIDESHOW_SLIDESHOW_SUCCEED,
        data,
    }),
    updateSlideshowFailed: (err) => ({
        type: UPDATE_SLIDESHOW_SLIDESHOW_FAILED,
        err,
    }),

    deleteSlideshow: (params, callback) => ({
        type: DELETE_SLIDESHOW,
        params,
        callback,
    }),
    deleteSlideshowSucceed: (data) => ({
        type: DELETE_SLIDESHOW_SLIDESHOW_SUCCEED,
        data,
    }),
    deleteSlideshowFailed: (err) => ({
        type: DELETE_SLIDESHOW_SLIDESHOW_FAILED,
        err,
    }),
};
