package com.viettel.mwin_cms.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "MWIN_AUCTION_PRODUCT")
public class MWinProduct {

    @Id
    @Column(name = "PRODUCT_ID", length = 200)
    private String productId;

    @Column(name = "PRODUCT_CODE", length = 200)
    private String productCode;

    @Column(name = "PRODUCT_NAME_EN", length = 500)
    private String productNameEn;

    @Column(name = "PRODUCT_NAME_LC", length = 500)
    private String productNameLc;

    @Column(name = "IMAGE_URL_EN", length = 4000)
    private String imageUrlEn;

    @Column(name = "IMAGE_URL_LC", length = 4000)
    private String imageUrlLc;

    @Column(name = "DESC_EN", length = 4000)
    private String descEn;

    @Column(name = "DESC_LC", length = 4000)
    private String descLc;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "PRICE")
    private BigDecimal price;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductNameEn() {
        return productNameEn;
    }

    public void setProductNameEn(String productNameEn) {
        this.productNameEn = productNameEn;
    }

    public String getProductNameLc() {
        return productNameLc;
    }

    public void setProductNameLc(String productNameLc) {
        this.productNameLc = productNameLc;
    }

    public String getImageUrlEn() {
        return imageUrlEn;
    }

    public void setImageUrlEn(String imageUrlEn) {
        this.imageUrlEn = imageUrlEn;
    }

    public String getImageUrlLc() {
        return imageUrlLc;
    }

    public void setImageUrlLc(String imageUrlLc) {
        this.imageUrlLc = imageUrlLc;
    }

    public String getDescEn() {
        return descEn;
    }

    public void setDescEn(String descEn) {
        this.descEn = descEn;
    }

    public String getDescLc() {
        return descLc;
    }

    public void setDescLc(String descLc) {
        this.descLc = descLc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
