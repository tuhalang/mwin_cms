import {connect} from 'react-redux';
import actions from '../../redux/actions/rules'
import Rules from "../../views/Rules";

const mapStateToProps = (state) => ({
    rules: state.rule.rules,
});

const mapDispatchToProps = (dispatch) => ({
    onFetchPaginateRule(params, callback) {
        dispatch(actions.fetchPaginateRule(params, callback));
    },
    onUpdateRule(params, callback) {
        dispatch(actions.updateRule(params, callback));
    },
    onCreateRule(params, callback) {
        dispatch(actions.createRule(params, callback));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Rules);
