import React, {useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusCircle} from "@fortawesome/free-solid-svg-icons";
import {AutoComplete, DatePicker, Form, Input, InputNumber, Popconfirm, Switch, Table, Typography, Button, Dropdown, Menu, Select} from "antd";
import { DownOutlined } from '@ant-design/icons';
import Modal from "antd/es/modal/Modal";
import consts from "../../consts";
import _ from 'lodash';
import {useTranslation} from "react-i18next";
import utils from "../../common/utils";
import moment from "moment";
import { FiberSmartRecord } from "@material-ui/icons";

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardCategoryBlack: {
        "&,& a,& a:hover,& a:focus": {
            color: 'black',
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardTitle: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    iconAdd: {
        marginRight: 4,
        fontSize: 18
    },
    btnAddSession: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-end',
        marginBottom: 8
    },
};

const useStyles = makeStyles(styles);
const {RangePicker} = DatePicker;

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

export default function Auctions(props) {
    const classes = useStyles();

    const [form] = Form.useForm();
    const {totalElements, totalPages} = props
    const [data, setData] = useState(props.sessions);
    const [editingKey, setEditingKey] = useState('');
    const [addAuction, setAddAuction] = useState(false);
    const [options, setOptions] = useState([]);
    const [productId, setproductId] = useState('');
    const [productId2, setproductId2] = useState('');
    const [page, setPage] = useState(1)
    const [loading, setLoading] = useState(false)
    const [autoCompleteDisabled, setAutoCompleteDisabled] = useState(false)
    const [updateSession, setUpdateSession] = useState(null)
    const [autoCompleteUpdateDisabled, setAutoCompleteUpdateDisabled] = useState(false)
    const [filter, setFilter] = useState("");
    
    const {t} = useTranslation('session')


    const cancel = () => {
        setEditingKey('');
    };

    const fetchData = (page = 1, size = consts.DEFAULT_PAGE_SIZE, status = 1, type=filter) => {
        props.onFetchPaginateSession({
            page,
            size,
            status,
            type
        }, () => {
            setLoading(false)
        })
    }

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '10%',
            editable: true,
        },
        {
            title: t('sessionCode'),
            dataIndex: 'sessionCode',
            width: '15%',
            editable: true,
        },
        {
            title: t('sessionType'),
            dataIndex: 'sessionType',
            width: '15%',
            editable: true,
        },
        {
            title: t('productCode1'),
            dataIndex: 'mWinProducts',
            width: '15%',
            editable: true,
            render: (_, record) => {
                if(record.mWinProducts.length > 0) 
                    return record.mWinProducts[0]['productCode'] + "-" + record.mWinProducts[0]['productNameEn']
                return null
            }
        },
        {
            title: t('productCode2'),
            dataIndex: 'mWinProducts',
            width: '15%',
            editable: true,
            render: (_, record) => {
                if(record.mWinProducts.length > 1) 
                    return record.mWinProducts[1]['productCode'] + "-" + record.mWinProducts[1]['productNameEn']
                return null
            }
        },
        {
            title: t('startDate'),
            dataIndex: 'startDate',
            width: '20%',
            editable: true,
            render: (_, record) => new Date(record.startDate).toLocaleDateString('en-US')
        },
        {
            title: t('endDate'),
            dataIndex: 'endDate',
            width: '20%',
            editable: true,
            render: (_, record) => new Date(record.endDate).toLocaleDateString('en-US')
        },
        {
            title: t('sessionNameEn'),
            dataIndex: 'sessionNameEn',
            width: '15%',
            editable: true,
        },
        {
            title: t('sessionNameLc'),
            dataIndex: 'sessionNameLc',
            width: '15%',
            editable: true,
        },
        {
            title: t('status'),
            dataIndex: 'status',
            width: '5%',
            editable: true,
            render: (_, record) => {
                return record.status == 1 ? <span style={{color: 'green'}}>{t('going_on')}</span> :
                record.status == 2 ?<span style={{color: 'red'}}>{t('expired')}</span> :
                <span style={{color: 'red'}}>{t('comming_soon')}</span>
            }
        },
        {
            title: t('repair'),
            dataIndex: 'repair',
            render: (_, record) => {
                return (
                    <Typography.Link disabled={editingKey !== ''} onClick={() => {
                        if(record.sessionType === 'M') {
                            setAutoCompleteUpdateDisabled(false)
                        }else{
                            setAutoCompleteUpdateDisabled(false)
                        }
                        setUpdateSession(record)
                    }}>
                        {t('repair')}
                    </Typography.Link>
                );
            },
        },
        {
            title: t('copy'),
            dataIndex: 'copy',
            render: (_, record) => {
                return (
                    <Typography.Link disabled={editingKey !== ''} onClick={() => {
                        setUpdateSession({
                            ...record,
                            sessionCode: ""
                        })
                        setproductId(record.mWinProducts[0]['productId'])
                        if(record.mWinProducts[1] !== undefined){
                            setproductId2(record.mWinProducts[1]['productId'])
                        }
                        setAddAuction(true)
                    }}>
                        {t('copy')}
                    </Typography.Link>
                );
            },
        },
    ];

    function handleChange(value) {
        if(value == 'M'){
            setAutoCompleteDisabled(false)
        }else{
            setAutoCompleteDisabled(false)
        }
      }
    
      function handleChangeFilter(value) {
        setFilter(value);
        fetchData(page, consts.DEFAULT_PAGE_SIZE, 1, value);
      }

      function handleChangeUpdate(value) {
        if(value == 'M'){
            setAutoCompleteUpdateDisabled(false)
        }else{
            setAutoCompleteUpdateDisabled(false)
        }
      }

    const onFinish = (values) => {
        props.onCreateSession({
            startDate: values.time[0],
            endDate: values.time[1],
            status: parseInt(values.status),
            sessionNameEn: values.sessionNameEn,
            sessionNameLc: values.sessionNameLc,
            sessionCode: values.sessionCode,
            sessionType: values.sessionType,
            mWinProductCodes: [productId, productId2]
        }, () => {
            utils.showNotification(t('notification'), t('createSession'), 'success')
            setAddAuction(false)
            setUpdateSession(null)
            setproductId(null)
            setproductId2(null)
            fetchData()
        })
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const onFinishUpdate = (values, auctionSessionId) => {
        props.onUpdateSession({
            auctionSessionId,
            startDate: values.time[0],
            endDate: values.time[1],
            status: parseInt(values.status),
            sessionNameEn: values.sessionNameEn,
            sessionNameLc: values.sessionNameLc,
            sessionCode: values.sessionCode,
            sessionType: values.sessionType,
            mWinProductCodes: [productId, productId2]
        }, () => {
            utils.showNotification(t('notification'), t('updateSession'), 'success')
            setUpdateSession(null)
            setproductId(null)
            setproductId2(null)
            fetchData()
        })
    };

    const onFinishUpdateFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    React.useEffect(() => {
        console.log(props.data)
    }, [props.data])


    const onSearch = (searchText) => {
        props.onSearchPaginateProduct({
            page: 1,
            size: consts.MAX_AUTOCOMPLETE,
            status: 1,
            key: searchText
        }, () => {
        })
    };

    const onSelect1 = (data, option) => {
        setproductId(option.key)
    };

    const onSelect2 = (data, option) => {
        setproductId2(option.key)
    };

    useEffect(() => {
        setOptions(_.map(props.products, option => {
            return {
                key: option.productId,
                value: option.productCode + " - " + option.productNameEn,
            }
        }))
    }, [props.products])

    useEffect(() => {
        setData(props.sessions)
    }, [props.sessions])

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <>
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="primary">
                            <div className={classes.cardTitle}>
                                <div>
                                    <h4 className={classes.cardTitleWhite}>{t('manageSession')}</h4>
                                </div>
                            </div>
                        </CardHeader>
                        <CardBody>
                            <div className={classes.btnAddSession}>
                                <Select defaultValue="" style={{ width: 220 }} onChange={handleChangeFilter}>
                                    <Select.Option value="">All</Select.Option> 
                                    <Select.Option value="D">Day</Select.Option> 
                                    <Select.Option value="W">Week</Select.Option>
                                    <Select.Option value="M">Month</Select.Option>
                                </Select>


                                <Button type="primary" onClick={() => {
                                    setAddAuction(true)
                                }}>
                                    <FontAwesomeIcon icon={faPlusCircle} className={classes.iconAdd}></FontAwesomeIcon>
                                    <span>{t('createSessionTitle')}</span>
                                </Button>
                            </div>

                            <Form form={form} component={false}>
                                <Table
                                    bordered
                                    scroll={{
                                        x: 'max-content',
                                    }}
                                    dataSource={_.map(data, (e, idx) => {
                                        return {
                                            ...e,
                                            no: (page - 1) * consts.DEFAULT_PAGE_SIZE + idx + 1,
                                            key: e.auctionSessionId,
                                        }
                                    })}
                                    columns={columns}
                                    rowClassName="editable-row"
                                    pagination={{
                                        defaultPageSize: consts.DEFAULT_PAGE_SIZE,
                                        current: page,
                                        showSizeChanger: false,
                                        total: totalElements,
                                        onChange: (p, pageSize) => {
                                            setPage(p)
                                            setLoading(true)
                                            fetchData(p, consts.DEFAULT_PAGE_SIZE, 1)
                                            cancel()
                                        },
                                        hideOnSinglePage: true,
                                        responsive: true
                                    }} bordered
                                    loading={loading}
                                />
                            </Form>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
            <Modal
                title={t('createSessionTitle')}
                centered
                visible={addAuction}
                destroyOnClose={true}
                onOk={() => {
                    setAddAuction(false);
                    setUpdateSession(null);
                }}
                onCancel={() => {
                    setAddAuction(false);
                    setUpdateSession(null);
                }}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="addAuction" key="submit" htmlType="submit" type={'primary'}>
                            {t('submitCreateBtn')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <Form
                    {...layout}
                    name="basic"
                    id={'addAuction'}
                    initialValues={{remember: true}}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        label={t('sessionCodeLable')}
                        name="sessionCode"
                        initialValue={updateSession != null ? updateSession.sessionCode : ''}
                        rules={[{required: true, message: t('sessionCodeRequire')}]}
                    >
                        <Input aria-label="sessionCode"
                               placeholder={t('sessionCode')}
                        />
                    </Form.Item>
                    <Form.Item
                        label={t('sessionTypeLable')}
                        name="sessionType"
                        initialValue={updateSession != null ? updateSession.sessionType : ''}
                        rules={[{required: true, message: t('sessionTypeRequire')}]}
                    >
                        <Select defaultValue="W" style={{ width: 120 }} onChange={handleChange}>
                            <Select.Option value="D">Day</Select.Option> 
                            <Select.Option value="W">Week</Select.Option>
                            <Select.Option value="M">Month</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label={t('productCode1Lable')}
                        name="productCode1"
                        initialValue={updateSession != null ? updateSession.mWinProducts.length>0?updateSession.mWinProducts[0]['productCode']+"-"+updateSession.mWinProducts[0]['productNameEn']:'':''}
                        rules={[{required: true, message: t('productCode1Require')}]}
                    >
                        <AutoComplete
                            options={options}
                            style={{width: 200}}
                            onSelect={onSelect1}
                            onSearch={onSearch}
                            placeholder={t('productCodePlaceHolder')}
                        />
                    </Form.Item>
                    <Form.Item
                        label={t('productCode2Lable')}
                        name="productCode2"
                        initialValue={updateSession != null ? updateSession.mWinProducts.length>1?updateSession.mWinProducts[1]['productCode']+"-"+updateSession.mWinProducts[1]['productNameEn']:'':''}
                        rules={[{required: false, message: t('productCode2Require')}]}
                    >
                        <AutoComplete
                            disabled={autoCompleteDisabled}
                            options={options}
                            style={{width: 200}}
                            onSelect={onSelect2}
                            onSearch={onSearch}
                            placeholder={t('productCodePlaceHolder')}
                        />
                    </Form.Item>
                    <Form.Item
                        label={t('timeSelect')}
                        name="time"
                        initialValue={updateSession != null ? [moment(updateSession.startDate), moment(updateSession.endDate)] : moment()}
                        rules={[{required: true, message: t('timeRequire')}]}
                    >
                        <RangePicker/>
                    </Form.Item>

                    <Form.Item
                        label={t('sessionNameEnLable')}
                        name="sessionNameEn"
                        initialValue={updateSession != null ? updateSession.sessionNameEn : ''}
                        rules={[{required: true, message: t('sessionNameEnRequire')}]}
                    >
                        <Input aria-label="sessionNameEn"
                               placeholder={t('sessionNameEn')}
                        />
                    </Form.Item>

                    <Form.Item
                        label={t('sessionNameLcLable')}
                        name="sessionNameLc"
                        initialValue={updateSession != null ? updateSession.sessionNameLc : ''}
                        rules={[{required: true, message: t('sessionNameLcRequire')}]}
                    >
                        <Input aria-label="sessionNameLc"
                               placeholder={t('sessionNameLc')}
                        />
                    </Form.Item>

                    <Form.Item
                        label={t('status')}
                        name="status"
                        initialValue="1"
                        rules={[{required: true, message: t('statusRequired')}]}
                    >
                        <Select defaultValue="1" style={{ width: 150 }} onChange={handleChange}>
                            <Select.Option value="1">Going on</Select.Option>
                            <Select.Option value="2">Expired</Select.Option>
                            <Select.Option value="3">Comming soon</Select.Option>
                        </Select>
                    </Form.Item>
                </Form>
            </Modal>
            <Modal
                title={t('updateSessionTitle')}
                centered
                destroyOnClose={true}
                visible={updateSession != null && !addAuction}
                onOk={() => setUpdateSession(null)}
                onCancel={() => setUpdateSession(null)}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="updateSession" key="submit" htmlType="submit" type={'primary'}>
                            {t('submitUpdateBtn')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <Form
                    {...layout}
                    name="basic"
                    id={'updateSession'}
                    initialValues={{remember: true}}
                    onFinish={(values) => onFinishUpdate(values, updateSession.auctionSessionId)}
                    onFinishFailed={onFinishUpdateFailed}
                >
                    <Form.Item
                        label={t('sessionCodeLable')}
                        name="sessionCode"
                        initialValue={updateSession != null ? updateSession.sessionCode : ''}
                        rules={[{required: true, message: t('sessionCodeRequire')}]}
                    >
                        <Input aria-label="sessionCode"
                               placeholder={t('sessionCode')}
                        />
                    </Form.Item>
                    <Form.Item
                        label={t('sessionTypeLable')}
                        name="sessionType"
                        initialValue={updateSession != null ? updateSession.sessionType : ''}
                        rules={[{required: true, message: t('sessionTypeRequire')}]}
                    >
                        <Select defaultValue="W" style={{ width: 120 }} onChange={handleChangeUpdate}>
                            <Select.Option valud="D">Day></Select.Option>
                            <Select.Option value="W">Week</Select.Option>
                            <Select.Option value="M">Month</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label={t('productCode1Lable')}
                        name="productCode1"
                        initialValue={updateSession != null ? updateSession.mWinProducts.length>0?updateSession.mWinProducts[0]['productCode']+"-"+updateSession.mWinProducts[0]['productNameEn']:'':''}
                        rules={[{required: true, message: t('productCode1Require')}]}
                    >
                        <AutoComplete
                            options={options}
                            style={{width: 200}}
                            onSelect={onSelect1}
                            onSearch={onSearch}
                            placeholder={t('productCodePlaceHolder')}
                        />
                    </Form.Item>
                    <Form.Item
                        label={t('productCode2Lable')}
                        name="productCode2"
                        initialValue={updateSession != null ? updateSession.mWinProducts.length>1?updateSession.mWinProducts[1]['productCode']+"-"+updateSession.mWinProducts[1]['productNameEn']:'':''}
                        rules={[{required: false, message: t('productCode2Require')}]}
                    >
                        <AutoComplete
                            disabled={autoCompleteUpdateDisabled}
                            options={options}
                            style={{width: 200}}
                            onSelect={onSelect2}
                            onSearch={onSearch}
                            placeholder={t('productCodePlaceHolder')}
                        />
                    </Form.Item>
                    <Form.Item
                        label={t('timeSelect')}
                        name="time"
                        initialValue={updateSession != null ? [moment(updateSession.startDate), moment(updateSession.endDate)] : moment()}
                        rules={[{required: true, message: 'Enter the time start auction !'}]}
                    >
                        <RangePicker/>
                    </Form.Item>

                    <Form.Item
                        label={t('sessionNameEnLable')}
                        name="sessionNameEn"
                        initialValue={updateSession != null ? updateSession.sessionNameEn : ''}
                        rules={[{required: true, message: t('sessionNameEnRequire')}]}
                    >
                        <Input aria-label="sessionNameEn"
                               placeholder={t('sessionNameEn')}
                        />
                    </Form.Item>

                    <Form.Item
                        label={t('sessionNameLcLable')}
                        name="sessionNameLc"
                        initialValue={updateSession != null ? updateSession.sessionNameLc : ''}
                        rules={[{required: true, message: t('sessionNameLcRequire')}]}
                    >
                        <Input aria-label="sessionNameLc"
                               placeholder={t('sessionNameLc')}
                        />
                    </Form.Item>

                    <Form.Item
                        label={t('status')}
                        name="status"
                        initialValue={updateSession != null ? updateSession.status + "" : ''}
                        rules={[{required: true, message: t('statusRequired')}]}
                    >
                        <Select defaultValue="1" style={{ width: 150 }} onChange={handleChange}>
                            <Select.Option value="1">Going on</Select.Option>
                            <Select.Option value="2">Expired</Select.Option>
                            <Select.Option value="3">Comming soon</Select.Option>
                        </Select>
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
}
