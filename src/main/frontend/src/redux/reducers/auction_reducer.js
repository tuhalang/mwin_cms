import {
    FETCH_PAGINATE_SESSION_SUCCEED,
    FETCH_WINNER_RESULT_SUCCEED,
    SEARCH_PAGINATE_PRODUCT_SUCCEED
} from "../actions/auction/action_types";
import {FETCH_PAGINATE_PRODUCT_SUCCEED} from "../actions/product/action_types";

const defaultParams = {
    products: [],
    sessions: [],
    winnerResult: [],
    winnerTotal: 0,
    totalElements: 0,
    totalPages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case SEARCH_PAGINATE_PRODUCT_SUCCEED:
            console.log(action.data, "data auction")
            return {
                ...state,
                products: action.data.items,
            }
        case FETCH_PAGINATE_SESSION_SUCCEED:
            console.log(action.data, "data session")
            return {
                ...state,
                sessions: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        case FETCH_WINNER_RESULT_SUCCEED:
            return {
                ...state,
                winnerResult: action.data.items,
                winnerTotal: action.data.totalElements
            }
        default:
            return {
                ...state,
            };
    }
};
