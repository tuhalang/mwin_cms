import {FETCH_HISTORY, FETCH_HISTORY_FAILED, FETCH_HISTORY_SUCCEED} from "./action_types";


export default {
    fetchHistory: (params, callback) => ({
        type: FETCH_HISTORY,
        params,
        callback,
    }),
    fetchHistorySucceed: (data) => ({
        type: FETCH_HISTORY_SUCCEED,
        data,
    }),
    fetchHistoryFailed: (err) => ({
        type: FETCH_HISTORY_FAILED,
        err,
    }),
}
