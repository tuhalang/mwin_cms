import React from "react";
import BaseRequest from './BaseRequest';

const schema = 'secure';
/**
 * key base on host:port
 */
export default class SlideshowRequest extends BaseRequest {

    /**
     * @param {string} params.key
     * @param {string} params.page
     * @param {string} params.size
     * @param {int} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    fetchPaginateSlideshow(params) {
        const url = `${schema}/slideshows`;
        return this.get(url, params);
    }

    /**
     * @param {Object} params
     * @param {Object} query
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    createSlideshow(params, query) {
        console.log("request")
        const url = `${schema}/slideshows`;
        return this.post(url, params, query);
    }

    /**
     * @param {Object} params
     * @param {Object} query
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    updateSlideshow(params, query) {
        const url = `${schema}/slideshows`;
        return this.put(url, params, query);
    }

    /**
     * @param {string} params.mWinProductId
     * @param {int} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    deleteSlideshow(params) {
        const url = `${schema}/slideshows`;
        return this.del(url, params);
    }
}
