package com.viettel.mwin_cms.service.impl;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.model.MWinProduct;
import com.viettel.mwin_cms.model.MWinRule;
import com.viettel.mwin_cms.repository.MWinRuleRepository;
import com.viettel.mwin_cms.service.MWinRuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class MWinRuleServiceImpl implements MWinRuleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MWinRuleServiceImpl.class);

    @Autowired
    MWinRuleRepository mWinRuleRepository;

    private boolean isValid(ResponseDTO responseDTO, MWinRule mWinRule) {
        return true;
    }

    @Override
    public void create(ResponseDTO responseDTO, MWinRule mWinRule) throws Exception {
        if (isValid(responseDTO, mWinRule)) {
            MWinRule oldRule = mWinRuleRepository.findById(mWinRule.getRuleCode()).orElse(null);
            if(oldRule != null){
                responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDTO.setMessage(Constant.MSG_RULE_CODE_IS_EXISTS);
                return;
            }
            mWinRuleRepository.saveAndFlush(mWinRule);
            responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
            responseDTO.setMessage(Constant.MSG_SUCCESS);
        }
    }

    @Override
    public void update(ResponseDTO responseDTO, MWinRule mWinRule) throws Exception {
        MWinRule oldMWinRule = mWinRuleRepository.findById(mWinRule.getRuleCode()).orElse(null);
        if (oldMWinRule != null) {
            if(!ObjectUtils.isEmpty(mWinRule.getDescEn())
                    && !mWinRule.getDescEn().equals(oldMWinRule.getDescEn())){
                oldMWinRule.setDescEn(mWinRule.getDescEn());
            }

            if(!ObjectUtils.isEmpty(mWinRule.getDescLc())
                    && !mWinRule.getDescLc().equals(oldMWinRule.getDescLc())){
                oldMWinRule.setDescLc(mWinRule.getDescLc());
            }

            mWinRuleRepository.saveAndFlush(oldMWinRule);
            responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
            responseDTO.setMessage(Constant.MSG_SUCCESS);
        } else {
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_NOT_EXISTS);
        }
    }

    @Override
    public void delete(ResponseDTO responseDTO, String mWinRuleId) throws Exception {

    }

    @Override
    public void restore(ResponseDTO responseDTO, String mWinRuleId) throws Exception {

    }

    @Override
    public void find(ResponseDTO responseDTO, Pageable pageable, String key) throws Exception {
        if(key == null){
            key = "";
        }
        Page<MWinRule> page = mWinRuleRepository.find(pageable, key);
        responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
        responseDTO.setMessage(Constant.MSG_SUCCESS);
        responseDTO.pushData("items", page.getContent());
        responseDTO.pushData("totalElements", page.getTotalElements());
        responseDTO.pushData("totalPages", page.getTotalPages());
    }
}
