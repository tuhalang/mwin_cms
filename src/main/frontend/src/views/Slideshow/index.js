import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {faPlusCircle} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Table, Form, Button, notification} from "antd";
import Modal from "antd/es/modal/Modal";
import consts, {DNS_IMAGE} from "../../consts";
import ModalEditSlideshow from "../../components/ModalEditSlideshow";
import utils from "../../common/utils";
import _ from 'lodash'
import {useTranslation} from "react-i18next";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";
import Card from "../../components/Card/Card";


const styles = {
    header_1: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: 16
    },
    title: {
        fontSize: 20,
        fontWeight: '700'
    },
    iconAdd: {
        marginRight: 4,
        fontSize: 18
    },
    thumbnail: {
        border: '1px solid #ddd',
        borderRadius: 4,
        padding: 5,
        width: 80,
        '&:hover': {
            boxShadow: '0 0 2px 1px rgba(0, 140, 186, 0.5)',
        }
    },
    preview: {
        objectFit: 'contain',
        height: '100%',
        width: '100%',
        marginTop: 20,
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
};

const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

const defaultSlideshow = {
    id: "",
    imageTitleEn: "",
    imageTitleLc: "",
    imageUrlEn: null,
    imageUrlLc: null,
    imageOrder: 0,
    status: 0,
}

export default function SlideShow(props) {
    const classes = useStyles();

    const [form] = Form.useForm();
    const {totalElements, totalPages, slideshows} = props
    const [editing, setEditing] = useState(false);
    const [dataEditing, setDataEditing] = useState({});
    const [addSlideshow, setAddSlideshow] = useState(false);
    const [page, setPage] = useState(1)
    const [loading, setLoading] = useState(false)
    const [preview, setPreview] = useState(null)
    const {t, i18n} = useTranslation('slideshow')

    const fetchData = (page = 1, size = consts.DEFAULT_PAGE_SIZE, status = 1, key = '') => {
        props.onFetchPaginateSlideshow({
            key,
            page: page,
            size: size,
            status: status,
        }, (data) => {
            setLoading(false)
        })
    }


    const cancel = () => {
        setEditing(false);
    };

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '5%',
            editable: true,
        },
        {
            title: t('imageTitleEn'),
            dataIndex: 'imageTitleEn',
            width: '15%',
            editable: true,
        },
        {
            title: t('imageTitleLc'),
            dataIndex: 'imageTitleLc',
            width: '15%',
            editable: true,
        },
        {
            title: t('imageUrlEn'),
            dataIndex: 'imageUrlEn',
            width: '10%',
            editable: true,
            render: (_, {imageUrlEn}) => {
                return imageUrlEn == '' || imageUrlEn == null ? <div/> :
                    <img className={classes.thumbnail} src={DNS_IMAGE + imageUrlEn}
                         onClick={() => setPreview(imageUrlEn)}/>
            }
        },
        {
            title: t('imageUrlLc'),
            dataIndex: 'imageUrlLc',
            width: '10%',
            editable: true,
            render: (_, {imageUrlLc}) => {
                return imageUrlLc == '' || imageUrlLc == null ? <div/> :
                    <img className={classes.thumbnail} src={DNS_IMAGE + imageUrlLc}
                         onClick={() => setPreview(imageUrlLc)}/>
            }
        },
        {
            title: t('imageOrder'),
            dataIndex: 'imageOrder',
            width: '8%',
            editable: true,
        },
        {
            title: t('updateDate'),
            dataIndex: 'updateDate',
            width: '10%',
            editable: true,
            render: (_, record) => new Date(record.updateDate).toLocaleDateString('en-US')
        },
        {
            title: t('createDate'),
            dataIndex: 'createDate',
            width: '8%',
            editable: true,
            render: (_, record) => new Date(record.createDate).toLocaleDateString('en-US')
        },
        {
            title: t('status'),
            dataIndex: 'status',
            width: '5%',
            editable: true,
            render: (_, record) => {
                if (record.status == 0) {
                    return <span style={{color: 'red'}}>{t('inactive')}</span>
                } else {
                    return <span style={{color: 'green'}}>{t('active')}</span>
                }
            }
        },
        {
            title: t('detail'),
            dataIndex: 'detail',
            render: (_, record) => {
                return <a onClick={() => {
                    setDataEditing(record)
                    setEditing(true)
                }}>{t('detail')}</a>
            }
        },
    ];

    const onFinish = (values) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    // React.useEffect(() => {
    //     fetchData(1, consts.DEFAULT_PAGE_SIZE, 1, '')
    // }, [])

    React.useEffect(() => {
        fetchData()
    }, [])

    React.useEffect(() => {
    }, [props.totalElements])

    return (

        <Card>
            <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>{t('titleSlideshow')}</h4>
            </CardHeader>
            <CardBody>
                <div className={classes.header_1}>
                    <Button type="primary" onClick={() => setAddSlideshow(true)}>
                        <FontAwesomeIcon icon={faPlusCircle} className={classes.iconAdd}></FontAwesomeIcon>
                        <span>{t('newSlideshow')}</span>
                    </Button>
                </div>
                <div className={'listProduct'}>
                    <Form form={form} component={false}>
                        <Table bordered
                               scroll={{
                                   x: 'max-content',
                               }}
                               dataSource={_.map(slideshows, (e, idx) => _.assign(e, {no: (page - 1) * consts.DEFAULT_PAGE_SIZE + idx + 1})) || []}
                               columns={columns}
                               rowClassName="editable-row"
                               pagination={{
                                   defaultPageSize: consts.DEFAULT_PAGE_SIZE,
                                   current: page,
                                   showSizeChanger: false,
                                   total: totalElements,
                                   onChange: (p, pageSize) => {
                                       setPage(p)
                                       setLoading(true)
                                       fetchData(p, consts.DEFAULT_PAGE_SIZE, 1)
                                       cancel()
                                   },
                                   hideOnSinglePage: true,
                                   responsive: true
                               }} bordered
                               loading={loading}
                        />
                    </Form>
                </div>
                {addSlideshow ? <Modal
                    title={t('newSlideshow')}
                    centered
                    visible={addSlideshow}
                    onOk={() => setAddSlideshow(false)}
                    onCancel={() => setAddSlideshow(false)}
                    width={820}
                    footer={[
                        <Form.Item {...tailLayout}>
                            <Button form="createSlideshow" key="submit" htmlType="submit" type={'primary'}>
                                New Slideshow
                            </Button>
                        </Form.Item>
                    ]}
                >
                    <ModalEditSlideshow dataProps={defaultSlideshow} callback={() => {
                        notification['success']({
                            message: t('notification'),
                            description:
                                t('createSlideshowSuccess'),
                        })
                        setAddSlideshow(false)
                        fetchData()
                    }} actionWithSlideshow={props.onCreateSlideshow} type={'createSlideshow'}/>
                </Modal> : <div/>}
                {editing ? <Modal title={t('slideshowDetail')}
                                  centered
                                  visible={true}
                                  onOk={() => setEditing(false)}
                                  onCancel={() => setEditing(false)}
                                  width={820}
                                  footer={[
                                      <Form.Item {...tailLayout}>
                                          <Button form="updateSlideshow" key="submit" htmlType="submit" type={'primary'}>
                                              {t('detail')}
                                          </Button>
                                      </Form.Item>
                                  ]}>
                    <ModalEditSlideshow dataProps={_.cloneDeep(dataEditing)} callback={() => {
                        setEditing(false)
                        notification['success']({
                            message: t('notification'),
                            description:
                                t('updateSuccess'),
                        })
                        fetchData()
                    }} actionWithSlideshow={props.onUpdateSlideshow} type={'updateSlideshow'}/>
                </Modal> : <div/>}
                <Modal
                    centered
                    visible={preview == null ? false : true}
                    onOk={() => setPreview(null)}
                    width={620}
                    onCancel={() => setPreview(null)}
                    footer={[]}>
                    <img src={DNS_IMAGE + preview} alt={'Preview'} className={classes.preview}/>
                </Modal>
            </CardBody>
        </Card>
    );
}
