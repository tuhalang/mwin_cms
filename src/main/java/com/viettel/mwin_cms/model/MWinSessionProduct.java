package com.viettel.mwin_cms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MWIN_AUCTION_SESSION_PRODUCT")
public class MWinSessionProduct {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "AUNCTION_SESSION_ID")
    private String sessionId;

    @Column(name = "PRODUCT_ID")
    private String productId;

    @Column(name = "STATUS")
    private Integer status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
