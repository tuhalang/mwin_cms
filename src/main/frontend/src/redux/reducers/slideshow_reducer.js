import {} from '../actions/user/action_types';
import {FETCH_PAGINATE_SLIDESHOW_SUCCEED} from "../actions/slideshow/action_types";

const defaultParams = {
    slideshows: [],
    totalElements: 0,
    totalPages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case FETCH_PAGINATE_SLIDESHOW_SUCCEED:
            console.log(action.data,"data")
            return {
                ...state,
                slideshows: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
