package com.viettel.mwin_cms.service.impl;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.model.MWinProduct;
import com.viettel.mwin_cms.repository.MWinProductRepository;
import com.viettel.mwin_cms.service.MWinProduceService;
import com.viettel.mwin_cms.util.FileUtil;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;

@Service
public class MWinProductServiceImpl implements MWinProduceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MWinProductServiceImpl.class);

    @Autowired
    MWinProductRepository mWinProductRepository;

    @Value("${app.files.path}")
    private String filesPath;

    private boolean isValid(ResponseDTO responseDTO, MWinProduct mWinProduct){
        mWinProduct.setProductId(null);
        return true;
    }

    @Override
    public void create(ResponseDTO responseDTO, MWinProduct mWinProduct) throws Exception {
        if(isValid(responseDTO, mWinProduct)){


            String id = mWinProductRepository.getID();
            mWinProduct.setProductId(id);

            String base64En = mWinProduct.getImageUrlEn();
            String base64Lc = mWinProduct.getImageUrlLc();

            String extEn = FileUtil.getExtFile(base64En);
            String extLc = FileUtil.getExtFile(base64Lc);

            mWinProduct.setImageUrlEn(base64En.substring(base64En.indexOf(",")+1));
            mWinProduct.setImageUrlLc(base64Lc.substring(base64Lc.indexOf(",")+1));

            String imageEn = FileUtil.generateFileName(mWinProduct.getProductId(), Constant.LANGUAGE_EN, extEn);
            String imageLc = FileUtil.generateFileName(mWinProduct.getProductId(), Constant.LANGUAGE_LC, extLc);
            FileUtil.base64ToFile(mWinProduct.getImageUrlEn(), filesPath + imageEn);
            FileUtil.base64ToFile(mWinProduct.getImageUrlLc(), filesPath + imageLc);

            mWinProduct.setImageUrlEn(imageEn);
            mWinProduct.setImageUrlLc(imageLc);
            mWinProduct.setStatus(1);


            mWinProductRepository.saveAndFlush(mWinProduct);
            responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
            responseDTO.setMessage(Constant.MSG_SUCCESS);
        }
    }

    @Override
    public void update(ResponseDTO responseDTO, MWinProduct mWinProduct) throws Exception {
        MWinProduct oldMWinProduct = mWinProductRepository.findById(mWinProduct.getProductId()).orElse(null);
        if(oldMWinProduct != null){
            if(!ObjectUtils.isEmpty(mWinProduct.getProductNameLc())){
                oldMWinProduct.setProductNameLc(mWinProduct.getProductNameLc());
            }

            if(!ObjectUtils.isEmpty(mWinProduct.getProductNameEn())){
                oldMWinProduct.setProductNameEn(mWinProduct.getProductNameEn());
            }

            if(!ObjectUtils.isEmpty(mWinProduct.getDescEn())){
                oldMWinProduct.setDescEn(mWinProduct.getDescEn());
            }

            if(!ObjectUtils.isEmpty(mWinProduct.getDescLc())){
                oldMWinProduct.setDescLc(mWinProduct.getDescLc());
            }

            if(!ObjectUtils.isEmpty(mWinProduct.getPrice()) && mWinProduct.getPrice().compareTo(BigDecimal.ZERO) == 1){
                oldMWinProduct.setPrice(mWinProduct.getPrice());
            }

            if(!ObjectUtils.isEmpty(mWinProduct.getImageUrlEn())
                    && (ObjectUtils.isEmpty(oldMWinProduct.getImageUrlEn()) || !oldMWinProduct.getImageUrlEn().equals(mWinProduct.getImageUrlEn()))){

                String base64En = mWinProduct.getImageUrlEn();
                String extEn = FileUtil.getExtFile(base64En);
                mWinProduct.setImageUrlEn(base64En.substring(base64En.indexOf(",")+1));

                String imageEn = FileUtil.generateFileName(mWinProduct.getProductId(), Constant.LANGUAGE_EN, extEn);
                FileUtil.base64ToFile(mWinProduct.getImageUrlEn(), filesPath + imageEn);
                oldMWinProduct.setImageUrlEn(imageEn);
            }

            if(!ObjectUtils.isEmpty(mWinProduct.getImageUrlLc())
                    && (ObjectUtils.isEmpty(oldMWinProduct.getImageUrlLc()) || !oldMWinProduct.getImageUrlLc().equals(mWinProduct.getImageUrlLc()))){

                String base64Lc = mWinProduct.getImageUrlLc();
                String extLc = FileUtil.getExtFile(base64Lc);
                mWinProduct.setImageUrlLc(base64Lc.substring(base64Lc.indexOf(",")+1));

                String imageLc = FileUtil.generateFileName(mWinProduct.getProductId(), Constant.LANGUAGE_LC, extLc);
                FileUtil.base64ToFile(mWinProduct.getImageUrlLc(), filesPath + imageLc);
                oldMWinProduct.setImageUrlLc(imageLc);
            }

            mWinProductRepository.saveAndFlush(oldMWinProduct);
            responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
            responseDTO.setMessage(Constant.MSG_SUCCESS);
        }else{
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_NOT_EXISTS);
        }
    }

    @Override
    public void delete(ResponseDTO responseDTO, String mWinProductId) throws Exception {
        MWinProduct oldMWinProduct = mWinProductRepository.findById(mWinProductId).orElse(null);
        if(oldMWinProduct != null && oldMWinProduct.getStatus() == 1){
            oldMWinProduct.setStatus(0);
            mWinProductRepository.saveAndFlush(oldMWinProduct);
        }else{
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_NOT_EXISTS);
        }
    }

    @Override
    public void restore(ResponseDTO responseDTO, String mWinProductId) throws Exception {
        MWinProduct oldMWinProduct = mWinProductRepository.findById(mWinProductId).orElse(null);
        if(oldMWinProduct != null && oldMWinProduct.getStatus() == 0){
            oldMWinProduct.setStatus(1);
            mWinProductRepository.saveAndFlush(oldMWinProduct);
        }else{
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_NOT_EXISTS);
        }
    }

    @Override
    public void find(ResponseDTO responseDTO, Pageable pageable, Integer status, String key) throws Exception {
        if(key == null){
            key = "";
        }
        Page<MWinProduct> page = mWinProductRepository.find(pageable, status, key);
        responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
        responseDTO.setMessage(Constant.MSG_SUCCESS);
        responseDTO.pushData("items", page.getContent());
        responseDTO.pushData("totalElements", page.getTotalElements());
        responseDTO.pushData("totalPages", page.getTotalPages());
    }
}
