build:
	mvn clean install

deploy:
	cp -r target/MWIN_CMS.war apache-tomcat/webapps; apache-tomcat/bin/startup.sh; tail -f apache-tomcat/logs/catalina.out

undeploy:
	apache-tomcat/bin/shutdown.sh; rm -rf apache-tomcat/webapps/MWIN_CMS*