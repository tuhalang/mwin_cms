/*eslint-disable*/
import React, {useState} from "react";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
import Hidden from "@material-ui/core/Hidden";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import moment from 'moment';
import styles from "assets/jss/cms/views/subCCStyle.js";
import {TextField} from "@material-ui/core";
import {AutoComplete, Button, DatePicker, Input, Table} from "antd";
import consts from "../../consts";
import utils from "../../common/utils";
import _ from "lodash";
import {useTranslation} from "react-i18next";

const {RangePicker} = DatePicker;

const useStyles = makeStyles(styles);

export default function SubCC(props) {
    const classes = useStyles();
    const [filter, setFilter] = useState({});
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(1);
    const {histories, historiesTotal} = props;
    const {t} = useTranslation('winner')

    console.log(histories,'histories')

    const onChangeDate = (value) => {
        if (value !== null) {
            let start = value[0].startOf('day').toString();
            let dateEnd = value[1].endOf('day').toString();
            filter.begin = parseInt(moment(start).valueOf() / 1000);
            filter.end = parseInt(moment(dateEnd).valueOf() / 1000);
        } else {
            filter.begin = '';
            filter.end = '';
        }
        // this.setState({ currentPage: 1, });
        // this.fetchData();
    }

    const onSearch = (page = 1, size = consts.DEFAULT_PAGE_SIZE) => {
        if (!filter.msisdn || filter.msisdn == '') {
            utils.showNotification('Notification', 'User code not found', 'error')
            return;
        }
        if (!(filter.begin && filter.end && filter.begin != '' && filter.end != '')) {
            utils.showNotification('Notification', 'Range time not found', 'error')
            return;
        }

        props.onFetchHistory({
            msisdn: filter.msisdn,
            page,
            size,
            start: filter.begin ? utils.convertDateFormat(filter.begin) : '',
            end: filter.end ? utils.convertDateFormat(filter.end) : '',
        })
    }

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '4%',
        },
        {
            title: t('msisdn'),
            dataIndex: 'msisdn',
            width: '7%',
        },
        {
            title: t('sessionName'),
            dataIndex: 'sessionName',
            width: '7%',
        },
        {
            title: t('auctionDate'),
            dataIndex: 'auctionDate',
            width: '7%',
        },
        {
            title: t('auctionPrice'),
            dataIndex: 'auctionPrice',
            width: '7%',
            render: (_, {auctionPrice}) => {
                return utils.convertPrice(auctionPrice)
            }
        },
        {
            title: t('playStatus'),
            dataIndex: 'playStatus',
            width: '6%',
        },
        {
            title: t('updateTime'),
            dataIndex: 'updateTime',
            width: '5%',
        },
        {
            title: t('requestDate'),
            dataIndex: 'requestDate',
            width: '6%',
        },
        {
            title: t('playType'),
            dataIndex: 'playType',
            width: '6%',
        },
        {
            title: t('auctionCode'),
            dataIndex: 'auctionCode',
            width: '6%',
        },
        {
            title: t('productName'),
            dataIndex: 'productName',
            width: '6%',
        },
        {
            title: t('playChannel'),
            dataIndex: 'playChannel',
            width: '6%',
        },
    ];


    return (
        <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
                <Card plain>
                    <CardHeader plain color="primary">
                        <h4 className={classes.cardTitleWhite}>History</h4>
                    </CardHeader>
                    <CardBody>
                        <div className={classes.filter}>
                            <Input
                                maxLength={50}
                                style={{
                                    maxWidth: 300,
                                    marginRight: 8,
                                }}
                                placeholder="Msisdn"
                                onChange={e => {
                                    filter.msisdn = e.target.value
                                    setFilter(filter)
                                }}
                            />
                            <div style={{display: 'flex', alignItems: 'center', marginRight: 8}}>
                                {/* <span className={classes.label}> Range time  </span> */}
                                <RangePicker
                                    locale={consts.LangDateVi}
                                    disabledDate={(current) => current && current > moment().endOf('day')}
                                    onChange={onChangeDate}
                                />
                            </div>
                        
                            <Button type="primary" onClick={() => onSearch()}>Search</Button>
                        </div>
                        <br></br>
                        <div className={classes.result}>
                            <Table
                                bordered
                                scroll={{
                                    x: 'max-content'
                                }}
                                dataSource={_.map(histories, (e, idx) => {
                                    return {
                                        ...e,
                                        no: (page - 1) * consts.DEFAULT_PAGE_SIZE + idx + 1,
                                        key: e.id,
                                    }
                                })}
                                columns={columns}
                                rowClassName="editable-row"
                                pagination={{
                                    defaultPageSize: consts.DEFAULT_PAGE_SIZE,
                                    current: page,
                                    showSizeChanger: false,
                                    total: historiesTotal,
                                    onChange: (p, pageSize) => {
                                        // setPage(p)
                                        // setLoading(true)
                                        // fetchData(p, consts.DEFAULT_PAGE_SIZE, 1)
                                        // cancel()
                                    },
                                    hideOnSinglePage: true,
                                    responsive: true
                                }} bordered
                                loading={loading}
                            />
                        </div>
                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
    );
}

const typeHistory = [
    {value: 'Lịch sử đăng kí', key: 1},
    {value: 'Lịch sử hủy', key: 2},
    {value: 'Lịch sử mua thêm', key: 2},
    {value: 'Lịch sử tham gia', key: 2},
]
