package com.viettel.mwin_cms.controller;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.model.MWinProduct;
import com.viettel.mwin_cms.service.MWinProduceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/secure/products")
public class MWinProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MWinProductController.class);

    @Autowired
    MWinProduceService mWinProduceService;

    @PostMapping
    public ResponseEntity create(@RequestBody MWinProduct mWinProduct){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            mWinProduceService.create(responseDTO, mWinProduct);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody MWinProduct mWinProduct){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            mWinProduceService.update(responseDTO, mWinProduct);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam Integer status,
                                @RequestParam(required = false) String key){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            mWinProduceService.find(responseDTO, pageable, status, key);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }

    @DeleteMapping
    public ResponseEntity delete(@RequestParam String mWinProductId,
                                 @RequestParam Integer type){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            if(type == 1) {
                mWinProduceService.delete(responseDTO, mWinProductId);
            }else{
                mWinProduceService.restore(responseDTO, mWinProductId);
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }
}
