package com.viettel.mwin_cms.dto;

import java.io.Serializable;
import java.util.Date;

public class PrizeResultDto implements Serializable {

    private String id;
    private String prizeType;
    private String msisdn;
    private String sessionName;
    private Date prizeDate;
    private Integer prizeStatus;
    private Integer auctionPrice;

    public PrizeResultDto(String id, String prizeType, String msisdn, String sessionName, Date prizeDate, Integer prizeStatus, Integer auctionPrice) {
        this.id = id;
        this.prizeType = prizeType;
        this.msisdn = msisdn;
        this.sessionName = sessionName;
        this.prizeDate = prizeDate;
        this.prizeStatus = prizeStatus;
        this.auctionPrice = auctionPrice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrizeType() {
        return prizeType;
    }

    public void setPrizeType(String prizeType) {
        this.prizeType = prizeType;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public Date getPrizeDate() {
        return prizeDate;
    }

    public void setPrizeDate(Date prizeDate) {
        this.prizeDate = prizeDate;
    }

    public Integer getPrizeStatus() {
        return prizeStatus;
    }

    public void setPrizeStatus(Integer prizeStatus) {
        this.prizeStatus = prizeStatus;
    }

    public Integer getAuctionPrice() {
        return auctionPrice;
    }

    public void setAuctionPrice(Integer auctionPrice) {
        this.auctionPrice = auctionPrice;
    }
}
