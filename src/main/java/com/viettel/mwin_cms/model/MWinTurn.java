package com.viettel.mwin_cms.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "MWIN_AUCTION_TURN")
public class MWinTurn {

    @Id
    @Column(name = "AUCTION_TURN_ID", length = 200)
    private String auctionTurnId;

    @Column(name = "MSISDN")
    private String msisdn;

    @Column(name = "AUCTION_SESSION_ID")
    private String auctionSessionId;

    @Column(name = "AUCTION_DATE")
    @Temporal(TemporalType.DATE)
    private Date auctionDate;

    @Column(name = "AUCTION_PRICE")
    private Long auctionPrice;

    @Column(name = "PLAY_STATUS")
    private Integer playStatus;

    @Column(name = "UPDATE_TIME")
    @Temporal(TemporalType.DATE)
    private Date updateTime;

    @Column(name = "REQUEST_DATE")
    @Temporal(TemporalType.DATE)
    private Date requestDate;

    @Column(name = "PLAY_TYPE")
    private String playType;

    @Column(name = "AUCTION_CODE")
    private String auctionCode;

    @Column(name = "PRODUCT_ID")
    private String productId;

    @Column(name = "PLAY_CHANNEL")
    private String playChannel;

    public String getAuctionTurnId() {
        return auctionTurnId;
    }

    public void setAuctionTurnId(String auctionTurnId) {
        this.auctionTurnId = auctionTurnId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getAuctionSessionId() {
        return auctionSessionId;
    }

    public void setAuctionSessionId(String auctionSessionId) {
        this.auctionSessionId = auctionSessionId;
    }

    public Date getAuctionDate() {
        return auctionDate;
    }

    public void setAuctionDate(Date auctionDate) {
        this.auctionDate = auctionDate;
    }

    public Long getAuctionPrice() {
        return auctionPrice;
    }

    public void setAuctionPrice(Long auctionPrice) {
        this.auctionPrice = auctionPrice;
    }

    public Integer getPlayStatus() {
        return playStatus;
    }

    public void setPlayStatus(Integer playStatus) {
        this.playStatus = playStatus;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getPlayType() {
        return playType;
    }

    public void setPlayType(String playType) {
        this.playType = playType;
    }

    public String getAuctionCode() {
        return auctionCode;
    }

    public void setAuctionCode(String auctionCode) {
        this.auctionCode = auctionCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPlayChannel() {
        return playChannel;
    }

    public void setPlayChannel(String playChannel) {
        this.playChannel = playChannel;
    }
}
