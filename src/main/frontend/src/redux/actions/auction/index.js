import {
    CREATE_SESSION,
    CREATE_SESSION_FAILED,
    CREATE_SESSION_SUCCEED,
    DELETE_SESSION,
    DELETE_SESSION_FAILED,
    DELETE_SESSION_SUCCEED,
    FETCH_PAGINATE_SESSION,
    FETCH_PAGINATE_SESSION_FAILED,
    FETCH_PAGINATE_SESSION_SUCCEED,
    FETCH_WINNER_RESULT, FETCH_WINNER_RESULT_FAILED, FETCH_WINNER_RESULT_SUCCEED,
    SEARCH_PAGINATE_PRODUCT,
    SEARCH_PAGINATE_PRODUCT_FAILED,
    SEARCH_PAGINATE_PRODUCT_SUCCEED,
    UPDATE_SESSION,
    UPDATE_SESSION_FAILED,
    UPDATE_SESSION_SUCCEED

} from './action_types';

export default {

    searchPaginateProduct: (params, callback) => ({
        type: SEARCH_PAGINATE_PRODUCT,
        params,
        callback,
    }),
    searchPaginateProductSucceed: (data) => ({
        type: SEARCH_PAGINATE_PRODUCT_SUCCEED,
        data,
    }),
    searchPaginateProductFailed: (err) => ({
        type: SEARCH_PAGINATE_PRODUCT_FAILED,
        err,
    }),

    fetchPaginateSession: (params, callback) => ({
        type: FETCH_PAGINATE_SESSION,
        params,
        callback,
    }),
    fetchPaginateSessionSucceed: (data) => ({
        type: FETCH_PAGINATE_SESSION_SUCCEED,
        data,
    }),
    fetchPaginateSessionFailed: (err) => ({
        type: FETCH_PAGINATE_SESSION_FAILED,
        err,
    }),

    createSession: (params, callback) => ({
        type: CREATE_SESSION,
        params,
        callback,
    }),
    createSessionSucceed: (data) => ({
        type: CREATE_SESSION_SUCCEED,
        data,
    }),
    createSessionFailed: (err) => ({
        type: CREATE_SESSION_FAILED,
        err,
    }),

    updateSession: (params, callback) => ({
        type: UPDATE_SESSION,
        params,
        callback,
    }),
    updateSessionSucceed: (data) => ({
        type: UPDATE_SESSION_SUCCEED,
        data,
    }),
    updateSessionFailed: (err) => ({
        type: UPDATE_SESSION_FAILED,
        err,
    }),

    deleteSession: (params, callback) => ({
        type: DELETE_SESSION,
        params,
        callback,
    }),
    deleteSessionSucceed: (data) => ({
        type: DELETE_SESSION_SUCCEED,
        data,
    }),
    deleteSessionFailed: (err) => ({
        type: DELETE_SESSION_FAILED,
        err,
    }),

    fetchWinnerResult: (params, callback) => ({
        type: FETCH_WINNER_RESULT,
        params,
        callback,
    }),
    fetchWinnerResultSucceed: (data) => ({
        type: FETCH_WINNER_RESULT_SUCCEED,
        data,
    }),
    fetchWinnerResultFailed: (err) => ({
        type: FETCH_WINNER_RESULT_FAILED,
        err,
    }),

};
