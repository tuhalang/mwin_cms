package com.viettel.mwin_cms.service;

import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.model.MWinProduct;
import com.viettel.mwin_cms.model.MWinRule;
import org.springframework.data.domain.Pageable;

public interface MWinProduceService {

    void create(ResponseDTO responseDTO, MWinProduct mWinProduct) throws Exception;
    void update(ResponseDTO responseDTO, MWinProduct mWinProduct) throws Exception;
    void delete(ResponseDTO responseDTO, String mWinProductId) throws Exception;
    void restore(ResponseDTO responseDTO, String mWinProductId) throws Exception;
    void find(ResponseDTO responseDTO, Pageable pageable, Integer status, String key) throws Exception;

}
