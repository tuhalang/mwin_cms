import {
    LOGIN_HISTORY_SUCCEED,
    USER_LIST_SUCCEED,
    USER_BALANCES_SUCCEED,
    CLEAR_USER_DATA,
    USER_INFO_SUCCEED,
    USER_LOGIN_HISTORY_SUCCEED,
    USER_LIST_ADDRESS_SUCCEED,
    USER_SYSTEM_LIST_SUCCEED,
    USER_SYSTEM_INFO_SUCCEED,
    LOGIN_NORMAL_SUCCEED,
    LOGOUT_SUCCEED
} from '../actions/user/action_types';

const defaultParams = {
    session: {},
    isAuthenticated: false,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case LOGIN_HISTORY_SUCCEED:
            return {
                ...state
            }
        case LOGIN_NORMAL_SUCCEED:
            window.$isAuthenticated = true;
            return {
                ...state,
                session: action.data,
                isAuthenticated: true,
            };
        case USER_LIST_SUCCEED:
            return {
                ...state
            }
        case LOGOUT_SUCCEED:
            return defaultParams;
        case CLEAR_USER_DATA:
            return defaultParams;
        case USER_BALANCES_SUCCEED:
            return {
                ...state
            }
        case USER_LOGIN_HISTORY_SUCCEED:
            return {
                ...state,
                userLoginHistory: action.data,
            };
        case USER_LIST_ADDRESS_SUCCEED:
            return {
                ...state,
                userAddressList: action.data,
            };
        case USER_SYSTEM_LIST_SUCCEED:
            return {
                ...state,
                userSystemList: action.data,
            };
        case USER_SYSTEM_INFO_SUCCEED:
            return {
                ...state,
                userSystemInfo: action.data,
            };
        default:
            return {
                ...state,
            };
    }
};
