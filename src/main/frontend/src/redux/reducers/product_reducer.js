import {} from '../actions/user/action_types';
import {FETCH_PAGINATE_PRODUCT_SUCCEED} from "../actions/product/action_types";

const defaultParams = {
    products: [],
    totalElements: 0,
    totalPages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case FETCH_PAGINATE_PRODUCT_SUCCEED:
            console.log(action.data,"data")
            return {
                ...state,
                products: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
