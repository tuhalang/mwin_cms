import {
    SEARCH_PRODUCT,
    SEARCH_PRODUCT_FAILED,
    SEARCH_PRODUCT_SUCCEED
} from "./action_types";

export default {

    searchProduct: (params, callback) => ({
        type: SEARCH_PRODUCT,
        params,
        callback,
    }),
    searchProductSucceed: (data) => ({
        type: SEARCH_PRODUCT_SUCCEED,
        data,
    }),
    searchProductFailed: (err) => ({
        type: SEARCH_PRODUCT_FAILED,
        err,
    }),
}
