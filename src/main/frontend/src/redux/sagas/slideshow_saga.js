import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import {CREATE_SLIDESHOW, FETCH_PAGINATE_SLIDESHOW, UPDATE_SLIDESHOW} from "../actions/slideshow/action_types";
import rf from "../../requests/RequestFactory";
import utils from "../../common/utils";
import consts, {BASE_URL} from "../../consts";
import actions from "../actions/slideshow";
import _ from 'lodash'

function* fetchPaginateSlideshow(action) {
    console.log('=== fetchPaginateSlideshow ===', action);
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('SlideshowRequest').fetchPaginateSlideshow(params), action.params);
        console.log(data.data, 'resp');

        if (data.errorCode != 0) {
            utils.showNotification('Slideshow', data.message, consts.TYPE_ERROR);
            yield put(actions.fetchPaginateSlideshowFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.fetchPaginateSlideshowSucceed(data.data));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.fetchPaginateSlideshowFailed(err));
    }
}

function* updateSlideshow(action) {
    try {
        const {
            data,
            status
        } = yield call((params, query) => rf.getRequest('SlideshowRequest').updateSlideshow(params, query), action.params.slideshow, action.params.params);

        if (data.errorCode != 0) {
            utils.showNotification('Slideshow', data.message, consts.TYPE_ERROR);
            yield put(actions.updateSlideshowFailed(new Error('Error Step 1')));
            return;
        }
        if (_.isFunction(action.callback)) action.callback();
        yield put(actions.updateSlideshowSucceed(data.data));
    } catch (err) {
        yield put(actions.updateSlideshowFailed(err));
    }
}

function* createSlideshow(action) {
    try {
        const {
            data,
            status
        } = yield call((params, query) => rf.getRequest('SlideshowRequest').createSlideshow(params, query), action.params.slideshow, action.params.params);

        if (data.errorCode != 0) {
            utils.showNotification('Slideshow', data.message, consts.TYPE_ERROR);
            yield put(actions.createSlideshowFailed(new Error('Error Step 1')));
            return;
        }

        if (_.isFunction(action.callback)) action.callback();
        yield put(actions.createSlideshowSucceed(data.data));
    } catch (err) {
        yield put(actions.createSlideshowFailed(err));
    }
}

function* watchAllProducts() {
    yield takeLatest(FETCH_PAGINATE_SLIDESHOW, fetchPaginateSlideshow);
    yield takeLatest(UPDATE_SLIDESHOW, updateSlideshow);
    yield takeLatest(CREATE_SLIDESHOW, createSlideshow);
}

export default function* rootSaga() {
    yield all([fork(watchAllProducts)]);
}
