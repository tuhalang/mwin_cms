package com.viettel.mwin_cms.repository;

import com.viettel.mwin_cms.model.MWinSessionProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MWinSessionProductRepository extends JpaRepository<MWinSessionProduct, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select sp from MWinSessionProduct sp, MWinProduct p where " +
                    "sp.productId = p.productId and sp.sessionId = :sessionId order by p.productNameEn"
    )
    List<MWinSessionProduct> findAllBySessionId(@Param("sessionId") String sessionId);
}
