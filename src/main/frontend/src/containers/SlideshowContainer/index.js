import {connect} from 'react-redux';
import Slideshow from "../../views/Slideshow";
import actions from '../../redux/actions/slideshow'
import {call} from "redux-saga/effects";

const mapStateToProps = (state) => ({
    slideshows: state.slideshow.slideshows,
    totalElements: state.slideshow.totalElements,
    totalPages: state.slideshow.totalPages
});

const mapDispatchToProps = (dispatch) => ({
    onFetchPaginateSlideshow: (params, callback) => {
        dispatch(actions.fetchPaginateSlideshow(params, callback))
    },
    onCreateSlideshow: (params, callback) => {
        dispatch(actions.createSlideshow(params, callback))
    },
    onUpdateSlideshow: (params, callback) => {
        dispatch(actions.updateSlideshow(params, callback))
    },
    onDeleteSlideshow: (params, callback) => {
        dispatch(actions.deleteSlideshow(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Slideshow);
