package com.viettel.mwin_cms.repository;

import com.viettel.mwin_cms.model.MWinTurn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface MWinTurnRepository extends JpaRepository<MWinTurn, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select new com.viettel.mwin_cms.dto.HistoryDTO(" +
                    "t.auctionSessionId, " +
                    "t.msisdn, " +
                    "s.sessionNameEn, " +
                    "t.auctionDate, " +
                    "t.auctionPrice, " +
                    "t.playStatus, " +
                    "t.updateTime, " +
                    "t.requestDate, " +
                    "t.playType, " +
                    "t.auctionCode, " +
                    "p.productNameEn, " +
                    "t.playChannel" +
                    ") " +
                    "from MWinTurn t, MWinSession s, MWinProduct p where " +
                    "t.auctionSessionId = s.auctionSessionId " +
                    "and t.productId = p.productId " +
                    "and t.msisdn = :msisdn " +
                    "and t.requestDate >= :fromDate " +
                    "and t.requestDate <= :endDate " +
                    "order by t.requestDate desc"
    )
    Page<MWinTurn> findByMsisdn(Pageable pageable, String msisdn, Date fromDate, Date endDate);
}
