import {FETCH_HISTORY_SUCCEED} from "../actions/history/action_types";

const defaultParams = {
    histories: [],
    historiesTotal: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case FETCH_HISTORY_SUCCEED:
            return {
                ...state,
                histories: action.data.items,
                historiesTotal: action.data.totalElements,
            }
        default:
            return {
                ...state,
            };
    }
};
