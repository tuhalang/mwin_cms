package com.viettel.mwin_cms.service;

import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.dto.SessionDTO;
import com.viettel.mwin_cms.model.MWinSession;
import org.springframework.data.domain.Pageable;

import java.util.Date;

public interface MWinSessionService {

    void create(ResponseDTO responseDTO, SessionDTO sessionDTO) throws Exception;
    void update(ResponseDTO responseDTO, SessionDTO sessionDTO) throws Exception;
    void delete(ResponseDTO responseDTO, String mWinSessionId) throws Exception;
    void restore(ResponseDTO responseDTO, String mWinSessionId) throws Exception;
    void findBySessionName(ResponseDTO responseDTO, Pageable pageable, Integer status, String key, String type) throws Exception;

    void findByRangeDate(ResponseDTO responseDTO, Pageable pageable, Integer status, Date start, Date end, String key, String type) throws Exception;
}
