package com.viettel.mwin_cms.controller;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.service.MWinPrizeResultService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api/secure/result")
public class MWinResultController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MWinResultController.class);

    @Autowired
    MWinPrizeResultService mWinPrizeResultService;

    @GetMapping
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam String sessionId){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            mWinPrizeResultService.findBySessionId(responseDTO, pageable, sessionId);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }
}
