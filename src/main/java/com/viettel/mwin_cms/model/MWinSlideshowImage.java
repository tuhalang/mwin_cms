package com.viettel.mwin_cms.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "MWIN_SLIDESHOW_IMAGE")
public class MWinSlideshowImage {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "IMAGE_TITLE")
    private String imageTitleEn;

    @Column(name = "IMAGE_URL_EN")
    private String imageUrlEn;

    @Column(name = "IMAGE_URL_LC")
    private String imageUrlLc;

    @Column(name = "IMAGE_TITLE_LC")
    private String imageTitleLc;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "CREATE_DATE")
    @CreationTimestamp
    private Date createDate;

    @Column(name = "IMAGE_ORDER")
    private Integer imageOrder;

    @Column(name = "UPDATE_DATE")
    @UpdateTimestamp
    private Date updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageTitleEn() {
        return imageTitleEn;
    }

    public void setImageTitleEn(String imageTitle) {
        this.imageTitleEn = imageTitle;
    }

    public String getImageUrlEn() {
        return imageUrlEn;
    }

    public void setImageUrlEn(String imageUrlEn) {
        this.imageUrlEn = imageUrlEn;
    }

    public String getImageUrlLc() {
        return imageUrlLc;
    }

    public void setImageUrlLc(String imageUrlLc) {
        this.imageUrlLc = imageUrlLc;
    }

    public String getImageTitleLc() {
        return imageTitleLc;
    }

    public void setImageTitleLc(String imageTitleLc) {
        this.imageTitleLc = imageTitleLc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getImageOrder() {
        return imageOrder;
    }

    public void setImageOrder(Integer imageOrder) {
        this.imageOrder = imageOrder;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
