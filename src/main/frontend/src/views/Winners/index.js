import React, {useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Button from "@material-ui/core/Button";
import Modal from "antd/es/modal/Modal";
import {useTranslation} from "react-i18next";
import {AutoComplete, Input, Table} from "antd";
import consts from "../../consts";
import _ from 'lodash';
import utils from "../../common/utils";

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgb(255,255,0)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    textFieldAuction: {
        color: 'white',
        borderColor: 'white',
    },
    autoCompleteAuction: {
        display: 'flex',
        alignItems: 'center',
    }
};

const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};


export default function Winners(props) {
    const classes = useStyles();
    const [options, setOptions] = useState([]);
    const [detailAuction, setDetailAuction] = React.useState(false)
    const {t} = useTranslation('winner')
    const [sessionIdSelected, setSessionId] = useState('');
    const {winnerTotal, totalPages} = props
    const [data, setData] = useState([])
    const [page, setPage] = useState(1)
    const [loading, setLoading] = useState(false)

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '4%',
        },
        {
            title: t('msisdn'),
            dataIndex: 'msisdn',
            width: '15%',
        },
        {
            title: t('auctionPrice'),
            dataIndex: 'auctionPrice',
            width: '25%',
            render: (_, {auctionPrice}) => {
                return utils.convertPrice(auctionPrice)
            }
        },
        {
            title: t('sessionName'),
            dataIndex: 'sessionName',
            width: '25%',
        },
        {
            title: t('prizeDate'),
            dataIndex: 'prizeDate',
            width: '10%',
            render: (_, record) => new Date(record.prizeDate).toLocaleDateString('en-US')
        },
        {
            title: t('prizeType'),
            dataIndex: 'prizeType',
            width: '6%',
        },
        {
            title: t('prizeStatus'),
            dataIndex: 'prizeStatus',
            width: '5%',
            render: (_, record) => {
                return record.prizeStatus == 1 ? <span style={{color: 'green'}}>{t('paid')}</span> :
                    <span style={{color: 'red'}}>{t('unpaid')}</span>
            }
        },
    ];

    const onSearch = (searchText) => {
        props.onFetchPaginateSession({
            page: 1,
            size: consts.MAX_AUTOCOMPLETE,
            status: 1,
            key: searchText,
        }, () => {
        })
    };

    const onSelect = (data, option) => {
        setSessionId(option.key)
        onFetchData(option.key);
    };

    const onFetchData = (sessionId = '', page = 1, size = consts.DEFAULT_PAGE_SIZE) => {
        props.onFetchWinnerResult({
            page,
            size,
            sessionId: sessionId,
        }, () => {
            console.log('fetch result success')
        })
    }

    React.useEffect(() => {
        setData(props.winnerResult)
    }, [props.winnerResult])


    useEffect(() => {
        console.log(props.sessions, 'props.sessions')
        setOptions(_.map(props.sessions, option => {
            return {
                key: option.auctionSessionId,
                value: option.sessionNameEn,
            }
        }))
    }, [props.sessions])

    return (
        <>
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="primary">
                            <h4 className={classes.cardTitleWhite}>{t('winnerTitle')}</h4>
                        </CardHeader>
                        <CardBody>
                            <Card>
                                <AutoComplete
                                    // dropdownMatchSelectWidth={252}
                                    style={{width: '100%'}}
                                    options={options}
                                    onSelect={onSelect}
                                    onSearch={onSearch}
                                >
                                    <Input.Search size="large" placeholder={t('search-placeholder')} enterButton/>
                                </AutoComplete>
                            </Card>
                            <Table
                                bordered
                                scroll={{
                                    x: 'max-content'
                                }}
                                dataSource={_.map(data, (e, idx) => {
                                    return {
                                        ...e,
                                        no: (page - 1) * consts.DEFAULT_PAGE_SIZE + idx + 1,
                                        key: e.id,
                                    }
                                })}
                                columns={columns}
                                rowClassName="editable-row"
                                pagination={{
                                    defaultPageSize: consts.DEFAULT_PAGE_SIZE,
                                    current: page,
                                    showSizeChanger: false,
                                    total: winnerTotal,
                                    onChange: (p, pageSize) => {
                                        // setPage(p)
                                        // setLoading(true)
                                        // fetchData(p, consts.DEFAULT_PAGE_SIZE, 1)
                                        // cancel()
                                    },
                                    hideOnSinglePage: true,
                                    responsive: true
                                }} bordered
                                loading={loading}
                            />
                        </CardBody>
                    </Card>

                </GridItem>
            </GridContainer>
            <Modal
                title="View detail session"
                centered
                visible={detailAuction}
                onOk={() => setDetailAuction(false)}
                onCancel={() => setDetailAuction(false)}
                footer={[
                    <Button form="addProduct" key="submit" htmlType="submit" type={'primary'}
                            onClick={() => setDetailAuction(false)}>
                        Close
                    </Button>
                ]}
            >
            </Modal>
        </>
    );
}
