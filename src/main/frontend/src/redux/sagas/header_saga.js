import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import utils from "../../common/utils";
import consts from "../../consts";
import actions from "../actions/header";
import _ from 'lodash';
import {SEARCH_PRODUCT} from "../actions/header/action_types";

function* searchPaginateProduct(action) {
    try {
        const {
            data,
            status
        } = yield call((params) => rf.getRequest('ProductRequest').fetchPaginateProduct(params), action.params);

        if (status === 400) {
            utils.showNotification('Product', data.message, consts.TYPE_ERROR);
            yield put(actions.searchProductFailed(new Error('Error Step 1')));
            return;
        }

        yield put(actions.searchProductSucceed(data.data));
        if (_.isFunction(action.callback)) action.callback()
    } catch (err) {
        yield put(actions.searchProductFailed(err));
    }
}

function* watchHeader() {
    yield takeLatest(SEARCH_PRODUCT, searchPaginateProduct);
}

export default function* rootSaga() {
    yield all([fork(watchHeader)]);
}
