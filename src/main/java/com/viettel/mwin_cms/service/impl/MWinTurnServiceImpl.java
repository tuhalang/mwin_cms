package com.viettel.mwin_cms.service.impl;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.model.MWinTurn;
import com.viettel.mwin_cms.repository.MWinTurnRepository;
import com.viettel.mwin_cms.service.MWinTurnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class MWinTurnServiceImpl implements MWinTurnService {

    @Autowired
    MWinTurnRepository mWinTurnRepository;

    @Override
    public void find(ResponseDTO responseDTO, Pageable pageable, String msisdn, Date fromDate, Date endDate) throws Exception {
        Page<MWinTurn> page = mWinTurnRepository.findByMsisdn(pageable, msisdn, fromDate, endDate);
        responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
        responseDTO.setMessage(Constant.MSG_SUCCESS);
        responseDTO.pushData("items", page.getContent());
        responseDTO.pushData("totalElements", page.getTotalElements());
        responseDTO.pushData("totalPages", page.getTotalPages());
    }
}
