package com.viettel.mwin_cms.service;

import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.model.MWinProduct;
import com.viettel.mwin_cms.model.MWinSlideshowImage;
import org.springframework.data.domain.Pageable;

public interface MWinSlideshowImageService {

    void create(ResponseDTO responseDTO, MWinSlideshowImage mWinSlideshowImage) throws Exception;
    void update(ResponseDTO responseDTO, MWinSlideshowImage mWinSlideshowImage) throws Exception;
    void delete(ResponseDTO responseDTO, String Id) throws Exception;
    void restore(ResponseDTO responseDTO, String Id) throws Exception;
    void find(ResponseDTO responseDTO, Pageable pageable, Integer status, String key) throws Exception;
}
