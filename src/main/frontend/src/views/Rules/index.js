import React, {useEffect, useState} from 'react';
import {Table, Input, InputNumber, Popconfirm, Form, Typography, Button, notification, DatePicker} from 'antd';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUserEdit} from "@fortawesome/free-solid-svg-icons";
import {makeStyles} from "@material-ui/core/styles";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";
import Card from "../../components/Card/Card";
import Modal from "antd/es/modal/Modal";
import {TextareaAutosize} from "@material-ui/core";
import consts from "../../consts";
import _ from 'lodash'
import utils from "../../common/utils";
import {useTranslation} from "react-i18next";


const EditableCell = ({
                          editing,
                          dataIndex,
                          title,
                          inputType,
                          record,
                          index,
                          children,
                          ...restProps
                      }) => {
    const inputNode = inputType === 'number' ? <InputNumber/> : <Input/>;
    return (
        <td {...restProps}>
            {editing ? (
                <Form.Item
                    name={dataIndex}
                    style={{
                        margin: 0,
                    }}
                    rules={[
                        {
                            required: true,
                            message: `Please Input ${title}!`,
                        },
                    ]}
                >
                    {inputNode}
                </Form.Item>
            ) : (
                children
            )}
        </td>
    );
};

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgb(255,255,0)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    textFieldAuction: {
        color: 'white',
        borderColor: 'white',
    },
    autoCompleteAuction: {
        display: 'flex',
        alignItems: 'center',
    },
    btnAddRules: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-end',
        marginBottom: 8
    },
    textArea: {
        maxHeight: 150,
        width: '100%',
        maxWidth: '100%',
        marginTop: 0,
        marginBottom: 0,
        overflowY: 'scroll !important',
    }
};

const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

const Rules = (props) => {
    const classes = useStyles();
    const [form] = Form.useForm();
    const [data, setData] = useState(_.map(props.rules, (value, key) => _.assign(value, {key: key})));
    const [editingKey, setEditingKey] = useState('');
    const [addRules, setAddRules] = useState(false);
    const {t} = useTranslation('rule')

    const isEditing = (record) => record.key === editingKey;

    const edit = (record) => {
        form.setFieldsValue({
            ruleCode: '',
            descLc: '',
            descEn: '',
            ...record,
        });
        setEditingKey(record.key);
    };

    const cancel = () => {
        setEditingKey('');
    };

    const save = async (key) => {
        try {
            const row = await form.validateFields();
            console.log(row, "row")
            const newData = [...data];
            const index = newData.findIndex((item) => key === item.key);
            if (index > -1) {
                props.onUpdateRule(_.omit({...newData[index], ...row}, ['key']), () => {
                    newData.splice(index, 1, {...newData[index], ...row});
                    setData(newData);
                    setEditingKey('');
                    utils.showNotification('Notification', 'Update rule success!', 'success')
                })
            }
        } catch (errInfo) {
            console.log('Validate Failed:', errInfo);
            utils.showNotification(t('notification'), t('validateRuleFailed'), 'error')
        }
    };

    const columns = [
        {
            title: t('ruleCode'),
            dataIndex: 'ruleCode',
            width: '10%',
            editable: true,
        },
        {
            title: t('descLc'),
            dataIndex: 'descLc',
            width: '40%',
            editable: true,
        },
        {
            title: t('descLc'),
            dataIndex: 'descEn',
            width: '40%',
            editable: true,
        },
        {
            title: t('update'),
            dataIndex: 'update',
            render: (_, record) => {
                const editable = isEditing(record);
                return editable ? (
                    <span>
            <a
                href="javascript:;"
                onClick={() => save(record.key)}
                style={{
                    marginRight: 8,
                }}
            >
              {t('save')}
            </a>
            <Popconfirm title={t('cancelQuestion')} onConfirm={cancel} cancelText={t('cancel')} okText={t('ok')}>
              <a>{t('close')}</a>
            </Popconfirm>
          </span>
                ) : (
                    <Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)}>
                        <FontAwesomeIcon icon={faUserEdit}/>
                    </Typography.Link>
                );
            },
        },
    ];
    const mergedColumns = columns.map((col) => {
            if (!col.editable) {
                return col;
            }

            return {
                ...col,
                onCell: (record) => ({
                    record,
                    inputType: 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: isEditing(record),
                }),
            };
        }
    );

    const onFinish = (values) => {
        props.onCreateRule(values, () => {
            setAddRules(false)
            setData([...data, _.assign(values, {key: data.length})])
        })
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const fetchData = (page = 1, size = consts.DEFAULT_PAGE_SIZE) => {
        props.onFetchPaginateRule({
            page,
            size,
        }, () => {
            console.log("fetch success")
        })
    }

    useEffect(() => {
        fetchData()
    }, [])

    useEffect(() => {
        setData(_.map(props.rules, (value, key) => _.assign(value, {key: key})))
    }, [props.rules])

    return (
        <>
            <Card>
                <CardHeader color="primary">
                    <h4 className={classes.cardTitleWhite}>{t('ruleTitle')}</h4>
                </CardHeader>
                <CardBody>
                    <div className={classes.btnAddRules}>
                        <Button type="primary" onClick={() => {
                            setAddRules(true)
                        }}>{t('newRule')}</Button>
                    </div>
                    <Form form={form} component={false}>
                        <Table
                            components={{
                                body: {
                                    cell: EditableCell,
                                },
                            }}
                            bordered
                            dataSource={data}
                            columns={mergedColumns}
                            rowClassName="editable-row"
                            pagination={{
                                onChange: cancel,
                            }}
                        />
                    </Form>
                </CardBody>
            </Card>
            <Modal
                title={t('newRule')}
                centered
                visible={addRules}
                onOk={() => setAddRules(false)}
                onCancel={() => setAddRules(false)}
                width={720}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="addAuction" key="submit" htmlType="submit" type={'primary'}>
                            {t('btnCreate')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <Form
                    {...layout}
                    name="basic"
                    id={'addAuction'}
                    initialValues={{remember: true}}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        label={t('ruleCode')}
                        name="ruleCode"
                        rules={[{required: true, message: t('ruleCodeRequire')}]}
                    >
                        <Input aria-label="ruleCode"
                               placeholder={t('ruleCode')}
                        />
                    </Form.Item>

                    <Form.Item
                        label={t('descLc')}
                        name="descLc"
                        rules={[{required: true, message: t('descLcRequire')}]}
                    >
                        <TextareaAutosize rowsMax={10}
                                          rowsMin={3}
                                          className={classes.textArea}
                                          aria-label="description"
                                          placeholder={t('descLc')}
                        />
                    </Form.Item>

                    <Form.Item
                        label={t('descEn')}
                        name="descEn"
                        rules={[{required: true, message: t('descEnRequire')}]}
                    >
                        <TextareaAutosize rowsMax={10}
                                          rowsMin={3}
                                          className={classes.textArea}
                                          aria-label="description"
                                          placeholder={t('descEn')}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
};


export default Rules;
