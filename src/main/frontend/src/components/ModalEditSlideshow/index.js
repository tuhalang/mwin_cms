import React, {useEffect, useState} from 'react';
import {Button, Form, Input, InputNumber, Modal, notification, Switch, Tooltip} from "antd";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faImage, faUpload} from "@fortawesome/free-solid-svg-icons";
import UploadFile from "../UploadFile";
import utils from "../../common/utils";
import {DNS_IMAGE} from "../../consts";
import {useTranslation} from "react-i18next";

const styles = {
    imageDiv: {
        display: 'flex',
        alignItems: 'center',
    },
    formItem: {
        margin: '8px 0 8px 0'

    },
    inputForm: {
        width: 450,
    }
};

const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function ModalEditSlideshow(props) {
    const classes = useStyles();
    const {
        dataProps,
        callback,
        actionWithSlideshow,
        type,
    } = props
    const {form} = Form.useForm();
    const {t} = useTranslation('slideshow')

    const columns = [
        {
            title: t('imageTitleEn'),
            type: 'string',
            name: 'imageTitleEn',
            value: dataProps['imageTitleEn']
        },
        {
            title: t('imageTitleLc'),
            type: 'string',
            name: 'imageTitleLc',
            value: dataProps['imageTitleLc']
        },
        {
            title: t('imageUrlEn'),
            type: 'image',
            name: 'imageUrlEn',
            value: dataProps['imageUrlEn']
        },
        {
            title: t('imageUrlLc'),
            type: 'image',
            name: 'imageUrlLc',
            value: dataProps['imageUrlLc']
        },
        {
            title: t('imageOrder'),
            type: 'number',
            name: 'imageOrder',
            editable: true,
        },
        {
            title: t('status'),
            type: 'switch',
            name: 'status',
            value: dataProps['status']
        }
    ]


    const [slideshow, setSlideshow] = useState({
        imageTitleEn: dataProps['imageTitleEn'] || '',
        imageTitleLc: dataProps['imageTitleLc'] || '',
        imageUrlEn: dataProps['imageUrlEn'] || 'image',
        imageUrlLc: dataProps['imageUrlLc'] || 'image',
        imageOrder: dataProps['imageOrder'] || '',
        status: dataProps['status'],
        id: dataProps['id'],
    })
    const [uploading, setUploading] = useState('')
    const [files, setFiles] = useState({})


    const [preview, setPreview] = useState(null)

    const onFinish = (values) => {
        let re = /(?:\.([^.]+))?$/;
        let extEn = re.exec(slideshow['imageUrlEn'])[1]
        let extLc = re.exec(slideshow['imageUrlLc'])[1]
        if (extEn == undefined || extLc == undefined) {
            notification['warning']({
                description: 'Image\'s format is invalid !',
                message: 'Notification'
            })
            return
        }
        _.forIn(files, (value, key) => {
            slideshow[key] = value
        })
        let result = {}
        _.forIn(slideshow, (value, key) => {
            if (key == 'id' || slideshow[key] != dataProps[key]) {
                result[key] = value
            }
        })
        actionWithSlideshow({
            params: {
                extEn,
                extLc,
            },
            slideshow: {
                ...result,
                status: result['status'] == true ? 1 : 0
            }
        }, () => {
            callback()
            setFiles({})
        })
    }

    return (
        <>
            <Form onFinish={onFinish} {...layout} name={type} form={form}
                  id={type}>
                {_.map(type == 'createSlideshow' ? _.remove(columns, o => o.name != 'id') : columns, e => {
                        const inputNode = e.type === 'number' ?
                            <InputNumber className={classes.inputForm} value={slideshow[e.name]} onChange={(value) => {
                                slideshow[e.name] = value
                                setSlideshow({
                                    ...slideshow,
                                })
                            }
                            } min={0}/> : e.type === 'string'
                                ? <Input className={classes.inputForm} value={slideshow[e.name]} onChange={(event) => {
                                    slideshow[e.name] = event.target.value
                                    setSlideshow({
                                        ...slideshow,
                                    })
                                }}/> : e.type === 'switch'
                                    ? <div>
                                        <Switch onChange={(checked) => setSlideshow({
                                            ...slideshow,
                                            status: checked == true ? 1 : 0
                                        })} defaultChecked={slideshow.status == 1 ? true : false}/>
                                        <span style={{
                                            color: 'rgba(0,0,0,0.4)',
                                            marginLeft: 8
                                        }}>{slideshow.status ? t('active') : t('inactive')}</span>
                                    </div> :
                                    <div className={classes.imageDiv}>
                                        <Input className={classes.inputForm} disabled={true} value={slideshow[e.name]}/>
                                        <Tooltip placement="right" title={t('preview')}>
                                            <FontAwesomeIcon icon={faImage} style={{fontSize: '22px', margin: 4}}
                                                             onClick={() => {
                                                                 setPreview(e.value)
                                                             }}/>
                                        </Tooltip>
                                        <Tooltip placement="right" title={t('upload')}>
                                            <FontAwesomeIcon icon={faUpload} style={{fontSize: '18px', margin: 4}}
                                                             onClick={() => {
                                                                 setUploading(e.name)
                                                             }}/>
                                        </Tooltip>
                                    </div>;
                        return (<Form.Item
                            className={classes.formItem}
                            name={e.name}
                            initialValue={slideshow[e.name]}
                            label={e.title}
                            rules={[{
                                required: true,
                                message: t('formItemRequire').replace("{title}", e.title),
                            }]}
                        >
                            {inputNode}
                        </Form.Item>)
                    }
                )}
            </Form>
            <Modal
                centered
                visible={preview == null ? false : true}
                onOk={() => setPreview(null)}
                onCancel={() => setPreview(null)}
                footer={[]}>
                <img src={DNS_IMAGE + preview} alt={t('preview')}/>
            </Modal>
            <Modal
                centered
                visible={uploading == '' ? false : true}
                onOk={() => setUploading('')}
                onCancel={() => setUploading('')}
                footer={[]}>
                <UploadFile type={uploading} callback={async (type, file, filePath) => {
                    files[type] = await utils.getBase64(file)
                    slideshow[type] = await filePath
                    setSlideshow({
                        ...slideshow
                    })
                    setUploading('')
                }}/>
            </Modal>
        </>
    )
}

export default ModalEditSlideshow;
