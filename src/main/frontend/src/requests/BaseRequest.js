import consts, {CONTEXT_PATH, BASE_URL} from "../consts";
import React from "react";
import _ from 'lodash'
import {notification} from "antd";
import utils from "../common/utils";

export default class BaseRequest {

    version = "MWIN_CMS/api";


    prefix() {
        return '';
    }

    async get(url, params = {}) {
        try {
            const response = await window.axios.get(`${this.version}/${url}`, {params});
            return this._responseHandler(response);
        } catch (error) {
            this._errorHandler(error);
        }
    }

    async getWithTimeout(url, params = {}, timeout) {
        try {
            const response = await window.axios.get(`${this.version}/${url}`, {params, timeout});
            return this._responseHandler(response);
        } catch (error) {
            this._errorHandler(error);
        }
    }

    async put(url, data = {}, query = {}) {
        try {
            const response = await window.axios.put(`${this.version}/${url}`, data, {params: query});
            return this._responseHandler(response);
        } catch (error) {
            this._errorHandler(error);
        }
    }

    async post(url, data = {}, query = {}) {
        console.log(`${this.version}/${url}`, 'url')
        try {
            const response = await window.axios.post(`${this.version}/${url}`, data, {params: query});
            return this._responseHandler(response);
        } catch (error) {
            this._errorHandler(error);
        }
    }

    async del(url, params = {}) {
        try {
            const response = await window.axios.delete(`${this.version}/${url}`, params);
            return this._responseHandler(response);
        } catch (error) {
            this._errorHandler(error);
        }
    }

    async _responseHandler(response) {
        const {data, status} = response;
        // if (status == 400) {
        //     notification['error']({
        //         message: 'Notification',
        //         description: "Request to server error",
        //     });
        //
        //     return Promise.reject(new Error(data.message || 'Error'))
        // }

        return {data, status};
    }

    _errorHandler(err) {
        if (err.response && err.response.status === 401) { // Unauthorized (session timeout)
            window.location.href = '/';
        } else if (err.response.status === 400) {
            utils.showNotification('Notification', "Request to server error", 'error')
        }
        throw err;
    }

    getFile(url) {
        window.location.href = `${BASE_URL}/${CONTEXT_PATH}/${url}`;
    }
}
