import {FETCH_PAGINATE_RULE_SUCCEED} from "../actions/rules/action_types";

const defaultParams = {
    rules: [],
    totalElements: 0,
    totalPages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case FETCH_PAGINATE_RULE_SUCCEED:
            console.log(action.data,"data")
            return {
                ...state,
                rules: action.data.items,
                // totalElements: action.data.totalElements,
                // totalPages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
