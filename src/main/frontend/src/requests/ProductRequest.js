import React from "react";
import BaseRequest from './BaseRequest';

const schema = 'secure';
/**
 * key base on host:port
 */
export default class ProductRequest extends BaseRequest {

    /**
     * @param {string} params.key
     * @param {string} params.page
     * @param {string} params.size
     * @param {int} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    fetchPaginateProduct(params) {
        const url = `${schema}/products`;
        return this.get(url, params);
    }

    /**
     * @param {Object} params
     * @param {Object} query
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    createProduct(params, query) {
        console.log("request")
        const url = `${schema}/products`;
        return this.post(url, params, query);
    }

    /**
     * @param {Object} params
     * @param {Object} query
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    updateProduct(params, query) {
        const url = `${schema}/products`;
        return this.put(url, params, query);
    }

    /**
     * @param {string} params.mWinProductId
     * @param {int} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    deleteProduct(params) {
        const url = `${schema}/products`;
        return this.del(url, params);
    }
}
