package com.viettel.mwin_cms.controller;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.model.MWinProduct;
import com.viettel.mwin_cms.model.MWinRule;
import com.viettel.mwin_cms.service.MWinRuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/secure/rules")
public class MwinRuleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MwinRuleController.class);

    @Autowired
    MWinRuleService mWinRuleService;

    @PostMapping
    public ResponseEntity create(@RequestBody MWinRule mWinRule){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            mWinRuleService.create(responseDTO, mWinRule);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody MWinRule mWinRule){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            mWinRuleService.update(responseDTO, mWinRule);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam(required = false) String key){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            mWinRuleService.find(responseDTO, pageable, key);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }
}
