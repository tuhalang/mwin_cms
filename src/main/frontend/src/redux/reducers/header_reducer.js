import {SEARCH_PRODUCT_SUCCEED} from "../actions/header/action_types";
import _ from 'lodash'

const defaultParams = {
    products: [],
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case SEARCH_PRODUCT_SUCCEED:
            return {
                ...state,
                products: _.map(action.data.items, e => {
                    return {
                        value: e.productNameEn,
                        key: e.productId,
                    }
                }),
            }
        default:
            return {
                ...state,
            };
    }
};
