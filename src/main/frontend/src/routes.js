import React from "react";
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import BubbleChart from "@material-ui/icons/BubbleChart";
import StoreMallDirectoryIcon from '@material-ui/icons/StoreMallDirectory';
import {faGavel, faAward, faList} from "@fortawesome/free-solid-svg-icons";
import DashboardPage from "./containers/DashBoardContainer";
import Product from "./containers/ProductContainer";
import Auctions from "./containers/AuctionContainer";
import Winners from "./containers/WinnerContainer";
import SubCC from "./containers/SubCCContainer";
import Rules from "./containers/RulesContainer";
import Slideshow from "./containers/SlideshowContainer";

const dashboardRoutes = [
    // {
    //     path: "/dashboard",
    //     name: "dashboard",
    //     rtlName: "Dashboard",
    //     icon: Dashboard,
    //     typeIcon: 'material',
    //     component: (props, data) => (<DashboardPage {...props}/>),
    //     layout: "/MWIN_CMS"
    // },
    {
        path: "/product",
        name: "productTitle",
        rtlName: "Manage products",
        icon: StoreMallDirectoryIcon,
        typeIcon: 'material',
        component: (props, data) => (<Product {...props} searchData={data}/>),
        layout: "/MWIN_CMS"
    },
    {
        path: "/auction",
        name: "sessionTitle",
        rtlName: "Manage sessions",
        icon: faGavel,
        typeIcon: 'fontawesome',
        component: (props, data) => (<Auctions {...props} data={data}/>),
        layout: "/MWIN_CMS"
    },
    {
        path: "/winners",
        name: "winnerTitle",
        rtlName: "Manage winners",
        icon: faAward,
        typeIcon: 'fontawesome',
        component: (props, data) => (<Winners {...props} data={data}/>),
        layout: "/MWIN_CMS"
    },
    {
        path: "/sub_cc",
        name: "subCCTitle",
        rtlName: "History",
        icon: BubbleChart,
        typeIcon: 'material',
        component: (props, data) => (<SubCC {...props} data={data}/>),
        layout: "/MWIN_CMS"
    },
    {
        path: "/rules",
        name: "ruleTitle",
        rtlName: "Manage rules",
        icon: faList,
        typeIcon: 'fontawesome',
        component: (props, data) => (<Rules {...props} data={data}/>),
        layout: "/MWIN_CMS"
    },
    {
        path: "/slideshow",
        name: "slideshowTitle",
        rtlName: "Manage slideshow",
        icon: StoreMallDirectoryIcon,
        typeIcon: 'material',
        component: (props, data) => (<Slideshow {...props} data={data}/>),
        layout: "/MWIN_CMS"
    },
];

export default dashboardRoutes;
