import {combineReducers} from 'redux';
import user from './user_reducer';
import dashboard from './dashboard_reducer';
import product from './product_reducer';
import auction from './auction_reducer'
import rule from './rule_reducer'
import header from './header_reducer';
import history from './history_reducer';
import slideshow from './slideshow_reducer';

const allReducers = combineReducers({
    user,
    dashboard,
    product,
    auction,
    rule,
    header,
    history,
    slideshow
});

export default allReducers;
