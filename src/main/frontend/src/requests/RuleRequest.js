import React from "react";
import BaseRequest from './BaseRequest';

const schema = 'secure';
/**
 * key base on host:port
 */
export default class RuleRequest extends BaseRequest {

    /**
     * @param {string} params.key
     * @param {string} params.page
     * @param {string} params.size
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    fetchPaginateRule(params) {
        const url = `${schema}/rules`;
        return this.get(url, params);
    }

    /**
     * @param {Object} params
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    createRule(params) {
        console.log("request")
        const url = `${schema}/rules`;
        return this.post(url, params);
    }

    /**
     * @param {Object} params
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    updateRule(params) {
        const url = `${schema}/rules`;
        return this.put(url, params);
    }

}
