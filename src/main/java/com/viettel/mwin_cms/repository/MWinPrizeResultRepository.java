package com.viettel.mwin_cms.repository;

import com.viettel.mwin_cms.dto.PrizeResultDto;
import com.viettel.mwin_cms.model.MWinPrizeResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface MWinPrizeResultRepository extends JpaRepository<MWinPrizeResult, String> {

    @Query(
            value = "select new com.viettel.mwin_cms.dto.PrizeResultDto(" +
                    "pr.id, pr.prizeType, pr.msisdn, s.sessionNameEn, pr.prizeDate, pr.prizeStatus, pr.auctionPrice ) " +
                    "from MWinPrizeResult pr, MWinSession s " +
                    "where pr.sessionId = s.auctionSessionId " +
                    "and s.auctionSessionId = :sessionId order by pr.prizeDate desc"
    )
    Page<PrizeResultDto> findBySessionId(Pageable pageable, @Param("sessionId") String sessionId);
}
