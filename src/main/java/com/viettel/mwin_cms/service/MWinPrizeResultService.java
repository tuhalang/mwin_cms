package com.viettel.mwin_cms.service;

import com.viettel.mwin_cms.dto.PrizeResultDto;
import com.viettel.mwin_cms.dto.ResponseDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MWinPrizeResultService {

    void findBySessionId(ResponseDTO responseDTO, Pageable pageable, String sessionId) throws Exception;
}
