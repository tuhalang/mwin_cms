export const FETCH_PAGINATE_SLIDESHOW = 'FETCH_PAGINATE_SLIDESHOW'
export const FETCH_PAGINATE_SLIDESHOW_SUCCEED = 'FETCH_PAGINATE_SLIDESHOW_SUCCEED'
export const FETCH_PAGINATE_SLIDESHOW_FAILED = 'FETCH_PAGINATE_SLIDESHOW_FAILED'

export const CREATE_SLIDESHOW = 'CREATE_SLIDESHOW_SLIDESHOW'
export const CREATE_SLIDESHOW_SUCCEED = 'CREATE_SLIDESHOW_SUCCEED'
export const CREATE_SLIDESHOW_FAILED = 'CREATE_SLIDESHOW_FAILED'


export const UPDATE_SLIDESHOW = 'UPDATE_SLIDESHOW_SLIDESHOW'
export const UPDATE_SLIDESHOW_SLIDESHOW_SUCCEED = 'UPDATE_SLIDESHOW_SLIDESHOW_SUCCEED'
export const UPDATE_SLIDESHOW_SLIDESHOW_FAILED = 'UPDATE_SLIDESHOW_SLIDESHOW_FAILED'

export const DELETE_SLIDESHOW = 'DELETE_SLIDESHOW_SLIDESHOW'
export const DELETE_SLIDESHOW_SLIDESHOW_SUCCEED = 'DELETE_SLIDESHOW_SLIDESHOW_SUCCEED'
export const DELETE_SLIDESHOW_SLIDESHOW_FAILED = 'DELETE_SLIDESHOW_SLIDESHOW_FAILED'
