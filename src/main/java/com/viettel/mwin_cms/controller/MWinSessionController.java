package com.viettel.mwin_cms.controller;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.dto.SessionDTO;
import com.viettel.mwin_cms.service.MWinSessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/api/secure/sessions")
public class MWinSessionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MWinSessionController.class);

    @Autowired
    MWinSessionService mWinSessionService;

    @PostMapping
    public ResponseEntity create(@RequestBody SessionDTO sessionDTO){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            mWinSessionService.create(responseDTO, sessionDTO);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody SessionDTO sessionDTO){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            mWinSessionService.update(responseDTO, sessionDTO);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam Integer status,
                                @RequestParam(required = false) String type,
                                @RequestParam(required = false) String key,
                                @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") Date start,
                                @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") Date end){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            if(ObjectUtils.isEmpty(start) || ObjectUtils.isEmpty(end)) {
                mWinSessionService.findBySessionName(responseDTO, pageable, status, key, type);
            }else{
                mWinSessionService.findByRangeDate(responseDTO, pageable, status, start, end, key, type);
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }

    @DeleteMapping
    public ResponseEntity delete(@RequestParam String mWinSessionId,
                                 @RequestParam Integer type){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            if(type == 1) {
                mWinSessionService.delete(responseDTO, mWinSessionId);
            }else{
                mWinSessionService.restore(responseDTO, mWinSessionId);
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }
}
