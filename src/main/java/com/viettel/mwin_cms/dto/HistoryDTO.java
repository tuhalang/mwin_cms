package com.viettel.mwin_cms.dto;

import java.util.Date;

public class HistoryDTO {

    private String auctionTurnId;

    private String msisdn;

    private String sessionName;

    private Date auctionDate;

    private Long auctionPrice;

    private Integer playStatus;

    private Date updateTime;

    private Date requestDate;

    private String playType;

    private String auctionCode;

    private String productName;

    private String playChannel;

    public HistoryDTO(String auctionTurnId, String msisdn, String sessionName, Date auctionDate, Long auctionPrice, Integer playStatus, Date updateTime, Date requestDate, String playType, String auctionCode, String productName, String playChannel) {
        this.auctionTurnId = auctionTurnId;
        this.msisdn = msisdn;
        this.sessionName = sessionName;
        this.auctionDate = auctionDate;
        this.auctionPrice = auctionPrice;
        this.playStatus = playStatus;
        this.updateTime = updateTime;
        this.requestDate = requestDate;
        this.playType = playType;
        this.auctionCode = auctionCode;
        this.productName = productName;
        this.playChannel = playChannel;
    }

    public String getAuctionTurnId() {
        return auctionTurnId;
    }

    public void setAuctionTurnId(String auctionTurnId) {
        this.auctionTurnId = auctionTurnId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public Date getAuctionDate() {
        return auctionDate;
    }

    public void setAuctionDate(Date auctionDate) {
        this.auctionDate = auctionDate;
    }

    public Long getAuctionPrice() {
        return auctionPrice;
    }

    public void setAuctionPrice(Long auctionPrice) {
        this.auctionPrice = auctionPrice;
    }

    public Integer getPlayStatus() {
        return playStatus;
    }

    public void setPlayStatus(Integer playStatus) {
        this.playStatus = playStatus;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getPlayType() {
        return playType;
    }

    public void setPlayType(String playType) {
        this.playType = playType;
    }

    public String getAuctionCode() {
        return auctionCode;
    }

    public void setAuctionCode(String auctionCode) {
        this.auctionCode = auctionCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPlayChannel() {
        return playChannel;
    }

    public void setPlayChannel(String playChannel) {
        this.playChannel = playChannel;
    }
}
