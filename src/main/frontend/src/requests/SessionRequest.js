import React from "react";
import BaseRequest from './BaseRequest';

const schema = 'secure';
/**
 * key base on host:port
 */
export default class SessionRequest extends BaseRequest {

    /**
     * @param {string} params.key
     * @param {string} params.page
     * @param {string} params.size
     * @param {int} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    fetchPaginateSession(params) {
        const url = `${schema}/sessions`;
        console.log(url)
        return this.get(url, params);
    }

    /**
     * @param {Object} params
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    createSession(params) {
        console.log("request")
        const url = `${schema}/sessions`;
        return this.post(url, params);
    }

    /**
     * @param {Object} params
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    updateSession(params) {
        const url = `${schema}/sessions`;
        return this.put(url, params);
    }

    /**
     * @param {string} params.mWinSessionId
     * @param {int} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    deleteSession(params) {
        const url = `${schema}/sessions`;
        return this.del(url, params);
    }

    /**
     * @param {string} params.page
     * @param {string} params.size
     * @param {int} params.sessionId
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    fetchWinnerResult(params) {
        const url = `${schema}/result`;
        console.log(url)
        return this.get(url, params);
    }
}
