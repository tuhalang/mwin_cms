package com.viettel.mwin_cms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MWIN_RULE")
public class MWinRule {

    @Id
    @Column(name = "RULE_CODE", length = 50, updatable = false)
    private String ruleCode;

    @Column(name = "DES_EN")
    private String descEn;

    @Column(name = "DES_LC")
    private String descLc;

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getDescEn() {
        return descEn;
    }

    public void setDescEn(String descEn) {
        this.descEn = descEn;
    }

    public String getDescLc() {
        return descLc;
    }

    public void setDescLc(String descLc) {
        this.descLc = descLc;
    }
}
