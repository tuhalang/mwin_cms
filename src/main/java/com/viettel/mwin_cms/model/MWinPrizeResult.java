package com.viettel.mwin_cms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "MWIN_PRIZE_RESULT")
public class MWinPrizeResult {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "PRIZE_TYPE")
    private String prizeType;

    @Column(name = "MSISDN")
    private String msisdn;

    @Column(name = "AUCTION_SESSION_ID")
    private String sessionId;

    @Column(name = "PRIZE_DATE")
    private Date prizeDate;

    @Column(name = "PRIZE_STATUS")
    private Integer prizeStatus;

    @Column(name = "AUCTION_PRICE")
    private Integer auctionPrice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrizeType() {
        return prizeType;
    }

    public void setPrizeType(String prizeType) {
        this.prizeType = prizeType;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getPrizeDate() {
        return prizeDate;
    }

    public void setPrizeDate(Date prizeDate) {
        this.prizeDate = prizeDate;
    }

    public Integer getPrizeStatus() {
        return prizeStatus;
    }

    public void setPrizeStatus(Integer prizeStatus) {
        this.prizeStatus = prizeStatus;
    }

    public Integer getAuctionPrice() {
        return auctionPrice;
    }

    public void setAuctionPrice(Integer auctionPrice) {
        this.auctionPrice = auctionPrice;
    }
}
