package com.viettel.mwin_cms.dto;

import com.viettel.mwin_cms.model.MWinProduct;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SessionDTO implements Serializable {

    private String auctionSessionId;
    private Date startDate;
    private Date endDate;
    private List<MWinProduct> mWinProducts;

    private List<String> mWinProductCodes;
    private Integer status;
    private String sessionNameEn;
    private String sessionNameLc;

    private String sessionCode;

    private String sessionType;

    public SessionDTO() {
    }

    public SessionDTO(String auctionSessionId, Date startDate, Date endDate, List<String> mWinProductCodes, Integer status, String sessionNameEn, String sessionNameLc, String sessionCode, String sessionType) {
        this.auctionSessionId = auctionSessionId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.mWinProductCodes = mWinProductCodes;
        this.status = status;
        this.sessionNameEn = sessionNameEn;
        this.sessionNameLc = sessionNameLc;
        this.sessionCode = sessionCode;
        this.sessionType = sessionType;
    }

    public SessionDTO(Date startDate, Date endDate, List<String> mWinProductCodes, Integer status, String sessionNameEn, String sessionNameLc, String sessionCode, String sessionType) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.mWinProductCodes = mWinProductCodes;
        this.status = status;
        this.sessionNameEn = sessionNameEn;
        this.sessionNameLc = sessionNameLc;
        this.sessionCode = sessionCode;
        this.sessionType = sessionType;
    }

    public SessionDTO(String auctionSessionId, Date startDate, Date endDate, Integer status, String sessionNameEn, String sessionNameLc, String sessionCode, String sessionType) {
        this.auctionSessionId = auctionSessionId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.sessionNameEn = sessionNameEn;
        this.sessionNameLc = sessionNameLc;
        this.sessionCode = sessionCode;
        this.sessionType = sessionType;
    }

    public SessionDTO(String auctionSessionId, Date startDate, Date endDate, Integer status) {
        this.auctionSessionId = auctionSessionId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
    }

    public List<String> getmWinProductCodes() {
        return mWinProductCodes;
    }

    public void setmWinProductCodes(List<String> mWinProductCodes) {
        this.mWinProductCodes = mWinProductCodes;
    }

    public String getSessionNameEn() {
        return sessionNameEn;
    }

    public void setSessionNameEn(String sessionNameEn) {
        this.sessionNameEn = sessionNameEn;
    }

    public String getSessionNameLc() {
        return sessionNameLc;
    }

    public void setSessionNameLc(String sessionNameLc) {
        this.sessionNameLc = sessionNameLc;
    }

    public String getSessionCode() {
        return sessionCode;
    }

    public void setSessionCode(String sessionCode) {
        this.sessionCode = sessionCode;
    }

    public String getSessionType() {
        return sessionType;
    }

    public void setSessionType(String sessionType) {
        this.sessionType = sessionType;
    }

    public List<MWinProduct> getmWinProducts() {
        return mWinProducts;
    }

    public void setmWinProducts(List<MWinProduct> mWinProducts) {
        this.mWinProducts = mWinProducts;
    }

    public String getAuctionSessionId() {
        return auctionSessionId;
    }

    public void setAuctionSessionId(String auctionSessionId) {
        this.auctionSessionId = auctionSessionId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


}
