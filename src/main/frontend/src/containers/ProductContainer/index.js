import {connect} from 'react-redux';
import Product from "../../views/Product";
import actions from '../../redux/actions/product'
import {call} from "redux-saga/effects";

const mapStateToProps = (state) => ({
    products: state.product.products,
    totalElements: state.product.totalElements,
    totalPages: state.product.totalPages
});

const mapDispatchToProps = (dispatch) => ({
    onFetchPaginateProduct: (params, callback) => {
        dispatch(actions.fetchPaginateProduct(params, callback))
    },
    onCreateProduct: (params, callback) => {
        dispatch(actions.createProduct(params, callback))
    },
    onUpdateProduct: (params, callback) => {
        dispatch(actions.updateProduct(params, callback))
    },
    onDeleteProduct: (params, callback) => {
        dispatch(actions.deleteProduct(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Product);
