package com.viettel.mwin_cms.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/secure/turns")
public class MWinTurnController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MWinTurnController.class);
}
