package com.viettel.mwin_cms.service.impl;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.model.MWinSlideshowImage;
import com.viettel.mwin_cms.repository.MWinSlideshowImageRepository;
import com.viettel.mwin_cms.service.MWinSlideshowImageService;
import com.viettel.mwin_cms.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class MWinSlideshowImageServiceImpl implements MWinSlideshowImageService {

    @Autowired
    MWinSlideshowImageRepository mWinSlideshowImageRepository;

    @Value("${app.files.path}")
    private String filesPath;

    private boolean isValid(ResponseDTO responseDTO, MWinSlideshowImage mWinSlideshowImage){
        mWinSlideshowImage.setId(null);
        return true;
    }

    @Override
    public void create(ResponseDTO responseDTO, MWinSlideshowImage mWinSlideshowImage) throws Exception {
        if(isValid(responseDTO, mWinSlideshowImage)){
            String id = mWinSlideshowImageRepository.getID();
            mWinSlideshowImage.setId(id);

            String base64En = mWinSlideshowImage.getImageUrlEn();
            String base64Lc = mWinSlideshowImage.getImageUrlLc();

            String extEn = FileUtil.getExtFile(base64En);
            String extLc = FileUtil.getExtFile(base64Lc);

            mWinSlideshowImage.setImageUrlEn(base64En.substring(base64En.indexOf(",")+1));
            mWinSlideshowImage.setImageUrlLc(base64Lc.substring(base64Lc.indexOf(",")+1));

            String imageEn = FileUtil.generateFileName(mWinSlideshowImage.getId(), Constant.LANGUAGE_EN, extEn);
            String imageLc = FileUtil.generateFileName(mWinSlideshowImage.getId(), Constant.LANGUAGE_LC, extLc);
            FileUtil.base64ToFile(mWinSlideshowImage.getImageUrlEn(), filesPath + imageEn);
            FileUtil.base64ToFile(mWinSlideshowImage.getImageUrlLc(), filesPath + imageLc);

            mWinSlideshowImage.setImageUrlEn(imageEn);
            mWinSlideshowImage.setImageUrlLc(imageLc);

            mWinSlideshowImage.setStatus(1);


            mWinSlideshowImageRepository.saveAndFlush(mWinSlideshowImage);
            responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
            responseDTO.setMessage(Constant.MSG_SUCCESS);
        }
    }

    @Override
    public void update(ResponseDTO responseDTO, MWinSlideshowImage mWinSlideshowImage) throws Exception {
        MWinSlideshowImage oldMWinSlideshowImage = mWinSlideshowImageRepository.findById(mWinSlideshowImage.getId()).orElse(null);
        if(oldMWinSlideshowImage != null){
            if(!ObjectUtils.isEmpty(mWinSlideshowImage.getImageTitleLc())){
                oldMWinSlideshowImage.setImageTitleLc(mWinSlideshowImage.getImageTitleLc());
            }

            if(!ObjectUtils.isEmpty(mWinSlideshowImage.getImageTitleEn())){
                oldMWinSlideshowImage.setImageTitleEn(mWinSlideshowImage.getImageTitleEn());
            }

            if(!ObjectUtils.isEmpty(mWinSlideshowImage.getImageOrder())){
                oldMWinSlideshowImage.setImageOrder(mWinSlideshowImage.getImageOrder());
            }

            if(!ObjectUtils.isEmpty(mWinSlideshowImage.getStatus())){
                oldMWinSlideshowImage.setStatus(mWinSlideshowImage.getStatus());
            }


            if(!ObjectUtils.isEmpty(mWinSlideshowImage.getImageUrlEn())
                    && (ObjectUtils.isEmpty(oldMWinSlideshowImage.getImageUrlEn()) || !oldMWinSlideshowImage.getImageUrlEn().equals(mWinSlideshowImage.getImageUrlEn()))){

                String base64En = mWinSlideshowImage.getImageUrlEn();
                String extEn = FileUtil.getExtFile(base64En);
                mWinSlideshowImage.setImageUrlEn(base64En.substring(base64En.indexOf(",")+1));

                String imageEn = FileUtil.generateFileName(oldMWinSlideshowImage.getId(), Constant.LANGUAGE_EN, extEn);
                FileUtil.base64ToFile(mWinSlideshowImage.getImageUrlEn(), filesPath + imageEn);
                oldMWinSlideshowImage.setImageUrlEn(imageEn);
            }

            if(!ObjectUtils.isEmpty(mWinSlideshowImage.getImageUrlLc())
                    && (ObjectUtils.isEmpty(oldMWinSlideshowImage.getImageUrlLc()) || !oldMWinSlideshowImage.getImageUrlLc().equals(mWinSlideshowImage.getImageUrlLc()))){

                String base64Lc = mWinSlideshowImage.getImageUrlLc();
                String extLc = FileUtil.getExtFile(base64Lc);
                mWinSlideshowImage.setImageUrlLc(base64Lc.substring(base64Lc.indexOf(",")+1));

                String imageLc = FileUtil.generateFileName(oldMWinSlideshowImage.getId(), Constant.LANGUAGE_LC, extLc);
                FileUtil.base64ToFile(mWinSlideshowImage.getImageUrlLc(), filesPath + imageLc);
                oldMWinSlideshowImage.setImageUrlLc(imageLc);
            }

            mWinSlideshowImageRepository.saveAndFlush(oldMWinSlideshowImage);
            responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
            responseDTO.setMessage(Constant.MSG_SUCCESS);
        }else{
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_NOT_EXISTS);
        }
    }

    @Override
    public void delete(ResponseDTO responseDTO, String id) throws Exception {
        MWinSlideshowImage oldMWinSlideshowImage = mWinSlideshowImageRepository.findById(id).orElse(null);
        if(oldMWinSlideshowImage != null && oldMWinSlideshowImage.getStatus() == 1){
            oldMWinSlideshowImage.setStatus(0);
            mWinSlideshowImageRepository.saveAndFlush(oldMWinSlideshowImage);
        }else{
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_NOT_EXISTS);
        }
    }

    @Override
    public void restore(ResponseDTO responseDTO, String id) throws Exception {
        MWinSlideshowImage oldMWinSlideshowImage = mWinSlideshowImageRepository.findById(id).orElse(null);
        if(oldMWinSlideshowImage != null && oldMWinSlideshowImage.getStatus() == 0){
            oldMWinSlideshowImage.setStatus(1);
            mWinSlideshowImageRepository.saveAndFlush(oldMWinSlideshowImage);
        }else{
            responseDTO.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDTO.setMessage(Constant.MSG_NOT_EXISTS);
        }
    }

    @Override
    public void find(ResponseDTO responseDTO, Pageable pageable, Integer status, String key) throws Exception {
        if(key == null){
            key = "";
        }
        Page<MWinSlideshowImage> page = mWinSlideshowImageRepository.find(pageable, key);
        responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
        responseDTO.setMessage(Constant.MSG_SUCCESS);
        responseDTO.pushData("items", page.getContent());
        responseDTO.pushData("totalElements", page.getTotalElements());
        responseDTO.pushData("totalPages", page.getTotalPages());
    }
}
