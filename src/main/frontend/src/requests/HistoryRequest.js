import React from "react";
import BaseRequest from './BaseRequest';

const schema = 'secure';
/**
 * key base on host:port
 */
export default class HistoryRequest extends BaseRequest {

    /**
     * @param {string} params.msisdn
     * @param {string} params.page
     * @param {string} params.size
     * @param {Date} params.start
     * @param {Date} params.end
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    fetchHistory(params) {
        const url = `${schema}/history`;
        return this.get(url, params);
    }

}
