package com.viettel.mwin_cms.controller;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.AccountDTO;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    UserService userService;


    @PostMapping("/signIn")
    public ResponseEntity signIn(@RequestBody AccountDTO accountDTO){
        ResponseDTO responseDTO = new ResponseDTO();
        try{
            userService.signIn(responseDTO, accountDTO);
        } catch (BadCredentialsException e){
            LOGGER.error(e.getMessage());
            responseDTO.setMessage(Constant.MSG_SIGN_IN_NOK);
        } catch (DisabledException e){
            LOGGER.error(e.getMessage());
            responseDTO.setMessage(Constant.MSG_USER_NOT_ACTIVE);
        } catch (Exception e){
            LOGGER.error(e.getMessage());
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }
}
