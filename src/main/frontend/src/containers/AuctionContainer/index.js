import {connect} from 'react-redux';
import actions from '../../redux/actions/auction'
import Auctions from "../../views/Auctions";

const mapStateToProps = (state) => ({
    products: state.auction.products,
    sessions: state.auction.sessions,
    totalElements: state.auction.totalElements,
    totalPages: state.auction.totalPages,
});

const mapDispatchToProps = (dispatch) => ({
    onSearchPaginateProduct: (params, callback) => {
        dispatch(actions.searchPaginateProduct(params, callback))
    },
    onFetchPaginateSession: (params, callback) => {
        dispatch(actions.fetchPaginateSession(params, callback))
    },
    onCreateSession: (params, callback) => {
        dispatch(actions.createSession(params, callback))
    },
    onUpdateSession: (params, callback) => {
        dispatch(actions.updateSession(params, callback))
    },
    onDeleteSession: (params, callback) => {
        dispatch(actions.deleteSession(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Auctions);
