package com.viettel.mwin_cms.service.impl;

import com.viettel.mwin_cms.common.Constant;
import com.viettel.mwin_cms.dto.PrizeResultDto;
import com.viettel.mwin_cms.dto.ResponseDTO;
import com.viettel.mwin_cms.repository.MWinPrizeResultRepository;
import com.viettel.mwin_cms.service.MWinPrizeResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MWinPrizeResultServiceImpl implements MWinPrizeResultService {
    @Autowired
    MWinPrizeResultRepository mWinPrizeResultRepository;

    @Override
    public void findBySessionId(ResponseDTO responseDTO, Pageable pageable, String sessionId) throws Exception {
        Page<PrizeResultDto> pages = mWinPrizeResultRepository.findBySessionId(pageable, sessionId);
        responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
        responseDTO.setMessage(Constant.MSG_SUCCESS);
        responseDTO.pushData("items", pages.getContent());
        responseDTO.pushData("totalElements", pages.getTotalElements());
        responseDTO.pushData("totalPages", pages.getTotalPages());
    }
}
