import {connect} from 'react-redux';
import Login from "../../layouts/Login";
import actions from '../../redux/actions/user'
import {withRouter} from "react-router";

const mapStateToProps = (state) => ({
    login: state.login
});

const mapDispatchToProps = (dispatch) => ({
    onLoginAction: (params, callback) => {
        dispatch(actions.loginHistoryAction(params, callback))
    },
    onLoginNormal: (params, callback) => {
        dispatch(actions.loginNormalAction(params, callback))
    }
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
