import React, {useEffect, useState} from 'react';
import {Button, Form, Input, InputNumber, Modal, notification, Switch, Tooltip} from "antd";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faImage, faUpload} from "@fortawesome/free-solid-svg-icons";
import UploadFile from "../UploadFile";
import utils from "../../common/utils";
import {DNS_IMAGE} from "../../consts";
import {useTranslation} from "react-i18next";

const styles = {
    imageDiv: {
        display: 'flex',
        alignItems: 'center',
    },
    formItem: {
        margin: '8px 0 8px 0'

    },
    inputForm: {
        width: 450,
    }
};

const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function ModalEditProduct(props) {
    const classes = useStyles();
    const {
        dataProps,
        callback,
        actionWithProduct,
        type,
    } = props
    const {form} = Form.useForm();
    const {t} = useTranslation('product')

    const columns = [
        {
            title: t('productCode'),
            type: 'string',
            name: 'productCode',
            value: dataProps['productCode']
        },
        {
            title: t('productNameEn'),
            type: 'string',
            name: 'productNameEn',
            value: dataProps['productNameEn']
        },
        {
            title: t('productNameLc'),
            type: 'string',
            name: 'productNameLc',
            value: dataProps['productNameLc']
        },
        {
            title: t('descEn'),
            type: 'string',
            name: 'descEn',
            value: dataProps['descEn']
        },
        {
            title: t('descLc'),
            type: 'string',
            name: 'descLc',
            value: dataProps['descLc']
        },
        {
            title: t('price'),
            type: 'number',
            name: 'price',
            value: dataProps['price']
        },
        {
            title: t('imageUrlEn'),
            type: 'image',
            name: 'imageUrlEn',
            value: dataProps['imageUrlEn']
        },
        {
            title: t('imageUrlLc'),
            type: 'image',
            name: 'imageUrlLc',
            value: dataProps['imageUrlLc']
        },
        {
            title: t('status'),
            type: 'switch',
            name: 'status',
            value: dataProps['status']
        }
    ]


    const [product, setProduct] = useState({
        descEn: dataProps['descEn'] || '',
        descLc: dataProps['descLc'] || '',
        imageUrlEn: dataProps['imageUrlEn'] || 'image',
        imageUrlLc: dataProps['imageUrlLc'] || 'image',
        price: dataProps['price'] != null && dataProps['price'] >= 0 ? dataProps['price'] : 0,
        productId: dataProps['productId'],
        productNameEn: dataProps['productNameEn'] || '',
        productNameLc: dataProps['productNameLc'] || '',
        status: dataProps['status'],
        productCode : dataProps['productCode']
    })
    const [uploading, setUploading] = useState('')
    const [files, setFiles] = useState({})


    const [preview, setPreview] = useState(null)

    const onFinish = (values) => {
        let re = /(?:\.([^.]+))?$/;
        let extEn = re.exec(product['imageUrlEn'])[1]
        let extLc = re.exec(product['imageUrlLc'])[1]
        if (extEn == undefined || extLc == undefined) {
            notification['warning']({
                description: 'Image\'s format is invalid !',
                message: 'Notification'
            })
            return
        }
        _.forIn(files, (value, key) => {
            product[key] = value
        })
        let result = {}
        _.forIn(product, (value, key) => {
            if (key == 'productId' || product[key] != dataProps[key]) {
                result[key] = value
            }
        })
        console.log(result, "result")
        actionWithProduct({
            params: {
                extEn,
                extLc,
            },
            mWinProduct: {
                ...result,
                status: result['status'] == true ? 1 : 0
            }
        }, () => {
            callback()
            setFiles({})
        })
    }

    return (
        <>
            <Form onFinish={onFinish} {...layout} name={type} form={form}
                  id={type}>
                {_.map(type == 'createProduct' ? _.remove(columns, o => o.name != 'productId') : columns, e => {
                        const inputNode = e.type === 'number' ?
                            <InputNumber className={classes.inputForm} value={product[e.name]} onChange={(value) => {
                                product[e.name] = value
                                setProduct({
                                    ...product,
                                })
                            }
                            } min={0}/> : e.type === 'string'
                                ? <Input className={classes.inputForm} value={product[e.name]} onChange={(event) => {
                                    product[e.name] = event.target.value
                                    setProduct({
                                        ...product,
                                    })
                                }}/> : e.type === 'switch'
                                    ? <div>
                                        <Switch onChange={(checked) => setProduct({
                                            ...product,
                                            status: checked == true ? 1 : 0
                                        })} defaultChecked={product.status == 1 ? true : false}/>
                                        <span style={{
                                            color: 'rgba(0,0,0,0.4)',
                                            marginLeft: 8
                                        }}>{product.status ? t('active') : t('inactive')}</span>
                                    </div> :
                                    <div className={classes.imageDiv}>
                                        <Input className={classes.inputForm} disabled={true} value={product[e.name]}/>
                                        <Tooltip placement="right" title={t('preview')}>
                                            <FontAwesomeIcon icon={faImage} style={{fontSize: '22px', margin: 4}}
                                                             onClick={() => {
                                                                 setPreview(e.value)
                                                             }}/>
                                        </Tooltip>
                                        <Tooltip placement="right" title={t('upload')}>
                                            <FontAwesomeIcon icon={faUpload} style={{fontSize: '18px', margin: 4}}
                                                             onClick={() => {
                                                                 setUploading(e.name)
                                                             }}/>
                                        </Tooltip>
                                    </div>;
                        return (<Form.Item
                            className={classes.formItem}
                            name={e.name}
                            initialValue={product[e.name]}
                            label={e.title}
                            rules={[{
                                required: true,
                                message: t('formItemRequire').replace("{title}", e.title),
                            }]}
                        >
                            {inputNode}
                        </Form.Item>)
                    }
                )}
            </Form>
            <Modal
                centered
                visible={preview == null ? false : true}
                onOk={() => setPreview(null)}
                onCancel={() => setPreview(null)}
                footer={[]}>
                <img src={DNS_IMAGE + preview} alt={t('preview')}/>
            </Modal>
            <Modal
                centered
                visible={uploading == '' ? false : true}
                onOk={() => setUploading('')}
                onCancel={() => setUploading('')}
                footer={[]}>
                <UploadFile type={uploading} callback={async (type, file, filePath) => {
                    files[type] = await utils.getBase64(file)
                    product[type] = await filePath
                    setProduct({
                        ...product
                    })
                    setUploading('')
                }}/>
            </Modal>
        </>
    )
}

export default ModalEditProduct;
